#ifndef _E_CAL_GLUE_RECUR_H_
#define _E_CAL_GLUE_RECUR_H_

#include <glib/gmacros.h>
#include <time.h>
#include <glib-object.h>
#include <libical/icalrecur.h>

typedef struct _ECalGlueRecurrence ECalGlueRecurrence;

struct _ECalGlueRecurrence {

  void* handle;

  /* Yearly/Monthly/Daily/Hourly/Minutely/Secondly */
  gint freq;
  
  /* Specifies the end of the recurrence, inclusive. No occurrences are
     generated after this date. If it is 0, the event recurs forever. 
  */
  time_t until;

  /* number of such recurrences
     NOTE:"until" and "count" cannot co-exist 
  */
  gint count;

  /* WKST property - the week start day: 0 = Monday to 6 = Sunday. */
  gint week_start_day;

  /* interval applied to freq */
  gshort interval;

  /* BY* parameters, according to RFC 2445 they all take a 
     list of values.
  */
    short by_second[ICAL_BY_SECOND_SIZE];
    short by_minute[ICAL_BY_MINUTE_SIZE];
    short by_hour[ICAL_BY_HOUR_SIZE];
    short by_day[ICAL_BY_DAY_SIZE]; /* Encoded value, see below */
    short by_month_day[ICAL_BY_MONTHDAY_SIZE];
    short by_year_day[ ICAL_BY_YEARDAY_SIZE];
    short by_week_no[ICAL_BY_WEEKNO_SIZE];
    short by_month[ICAL_BY_MONTH_SIZE];
    short by_set_pos[ICAL_BY_SETPOS_SIZE];
};


void* e_cal_glue_recurrence_get_handle          (ECalGlueRecurrence* comp);
gint e_cal_glue_recurrence_get_frequency        (ECalGlueRecurrence* comp);
gint e_cal_glue_recurrence_get_count            (ECalGlueRecurrence* comp);
time_t e_cal_glue_recurrence_get_enddate        (ECalGlueRecurrence* comp);
gint e_cal_glue_recurrence_get_week_start_day   (ECalGlueRecurrence* comp);
gshort e_cal_glue_recurrence_get_interval       (ECalGlueRecurrence* comp);
short* e_cal_glue_recurrence_get_by_second      (ECalGlueRecurrence* comp, int* array_len);
short* e_cal_glue_recurrence_get_by_minute      (ECalGlueRecurrence* comp, int* array_len);
short* e_cal_glue_recurrence_get_by_hour        (ECalGlueRecurrence* comp, int* array_len);
short* e_cal_glue_recurrence_get_by_day         (ECalGlueRecurrence* comp, int* array_len);
short* e_cal_glue_recurrence_get_by_month_day   (ECalGlueRecurrence* comp, int* array_len);
short* e_cal_glue_recurrence_get_by_year_day    (ECalGlueRecurrence* comp, int* array_len);
short* e_cal_glue_recurrence_get_by_week_number (ECalGlueRecurrence* comp, int* array_len);
short* e_cal_glue_recurrence_get_by_month       (ECalGlueRecurrence* comp, int* array_len);
short* e_cal_glue_recurrence_get_by_set_pos     (ECalGlueRecurrence* comp, int* array_len);

#endif

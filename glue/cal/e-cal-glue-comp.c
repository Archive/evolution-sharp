#include <stdio.h>

#include "e-cal-glue-comp.h"

int
e_cal_component_get_priority2 (ECalComponent* comp)
{
   g_return_val_if_fail (comp != NULL, 0);
   
   int priority = 0;
   int* p = 0;
   
   e_cal_component_get_priority (comp, &p);
   
   if (p) {
      priority = *p;
      e_cal_component_free_priority (p);
   }
   
   return priority;
}

time_t
e_cal_icaltimetype_to_timet (const struct icaltimetype* t, int freemem)
{
   g_return_val_if_fail (t != NULL, 0);
   
   time_t ret = icaltime_as_timet_with_zone (*t, icaltime_get_timezone (*t));
   
   if (freemem)
      e_cal_component_free_icaltimetype ((struct icaltimetype*) t);
   
   return ret;
}

time_t
e_cal_ecalcomponentdatetime_to_timet (ECalComponentDateTime* t, int freemem)
{
   g_return_val_if_fail (t != NULL, 0);
   
   time_t ret = 0;
   
   if (t->value)
      ret = icaltime_as_timet_with_zone (*(t->value), icaltime_get_timezone (*(t->value)));
   
   if (freemem)
      e_cal_component_free_datetime (t);
   
   return ret;
}

ECalComponentDateTime*
e_cal_alloc_ecalcomponentdatetime ()
{
   return g_new0 (ECalComponentDateTime, 1);
}

void
e_cal_free_ecalcomponentdatetime (ECalComponentDateTime* dt)
{
   g_return_if_fail (dt != NULL);
   
   g_free (dt);
}

struct icaltimetype*
e_cal_alloc_icaltimetype ()
{
   return g_new0 (struct icaltimetype, 1);
}

void
e_cal_free_icaltimetype (struct icaltimetype* dt)
{
   g_return_if_fail (dt != NULL);
   
   g_free (dt);
}

void
e_cal_glue_set_itt_month (struct icaltimetype* dt, int month)
{
   g_return_if_fail (dt != NULL);

   dt->month = month; 
}

int
e_cal_glue_get_itt_month (struct icaltimetype* dt)
{
   return dt->month; 
}

void
e_cal_glue_set_itt_year (struct icaltimetype* dt, int year)
{
   g_return_if_fail (dt != NULL);

   dt->year = year; 
}

int
e_cal_glue_get_itt_year (struct icaltimetype* dt)
{
   return dt->year; 
}


void
e_cal_glue_set_itt_day (struct icaltimetype* dt, int day)
{
   g_return_if_fail (dt != NULL);

   dt->day = day; 
}

int
e_cal_glue_get_itt_day (struct icaltimetype* dt)
{
   return dt->day; 
}

void
e_cal_glue_set_itt_hour (struct icaltimetype* dt, int hour)
{
   g_return_if_fail (dt != NULL);

   dt->hour = hour; 
}

int
e_cal_glue_get_itt_hour (struct icaltimetype* dt)
{
   return dt->hour; 
}

void
e_cal_glue_set_itt_minute (struct icaltimetype* dt, int minute)
{
   g_return_if_fail (dt != NULL);

   dt->minute = minute; 
}

int
e_cal_glue_get_itt_minute (struct icaltimetype* dt)
{
   return dt->minute; 
}

void
e_cal_glue_set_itt_second (struct icaltimetype* dt, int second)
{
   g_return_if_fail (dt != NULL);

   dt->second = second; 
}

int
e_cal_glue_get_itt_second (struct icaltimetype* dt)
{
   return dt->second; 
}

void
e_cal_glue_set_itt_is_utc (struct icaltimetype* dt, int is_utc)
{
   g_return_if_fail (dt != NULL);

   dt->is_utc = is_utc; 
}

int
e_cal_glue_get_itt_is_utc (struct icaltimetype* dt)
{
   return dt->is_utc; 
}

void
e_cal_glue_set_itt_is_date (struct icaltimetype* dt, int is_date)
{
   g_return_if_fail (dt != NULL);

   dt->is_date = is_date; 
}

int
e_cal_glue_get_itt_is_date (struct icaltimetype* dt)
{
   return dt->is_date; 
}

void
e_cal_glue_set_itt_is_daylight (struct icaltimetype* dt, int is_daylight)
{
   g_return_if_fail (dt != NULL);

   dt->is_daylight = is_daylight; 
}

int
e_cal_glue_get_itt_is_daylight (struct icaltimetype* dt)
{
   return dt->is_daylight; 
}

void
e_cal_glue_set_itt_zone (struct icaltimetype* dt, icaltimezone *zone)
{
   g_return_if_fail (dt != NULL);

   dt->zone = zone; 
}

icaltimetype *
e_cal_glue_get_itt_zone (struct icaltimetype* dt)
{
   return dt->zone; 
}

struct icaltimetype*
e_cal_timet_to_icaltimetype (int timet)
{
   struct icaltimetype* p = g_new (struct icaltimetype, 1);
   *p = icaltime_from_timet (timet, 0);
   return p;
}

ECalComponentDateTime*
e_cal_timet_to_ecalcomponentdatetime (int timet)
{
   ECalComponentDateTime* dt = g_new (ECalComponentDateTime, 1);
   
   dt->value = e_cal_timet_to_icaltimetype (timet);
   dt->tzid = g_strdup ("");
   
   return dt;
}

void
e_cal_set_icaltimetype_to_ecalcomponentdatetime (ECalComponentDateTime *dt, icaltimetype *p)
{
	dt->value = p;	
}

void
e_cal_glue_free_icaltimetype (struct icaltimetype* ical)
{
   g_return_if_fail (ical != NULL);
   
   g_free (ical);
}

void 
e_cal_glue_free_ecalcomponentdatetime (ECalComponentDateTime* dt)
{
   g_return_if_fail (dt != NULL);
   
   g_free (dt->value);
   g_free ((char*)dt->tzid);
   g_free (dt);
}

GSList*
e_cal_glue_add_string_to_glib_string_list (GSList* list, const char* str)
{
   g_return_val_if_fail (str != NULL, list);

   return g_slist_append (list, g_strdup (str));
}

void
e_cal_glue_free_glib_string_list (GSList* list)
{
   GSList* l = NULL;
   
   for (l = list; l; l = l->next)
      if (l->data)
         g_free (l->data);
   
   g_slist_free (list);
}

GSList*
e_cal_glue_add_calcomponenttext_to_gslist (GSList* list, const char* value, const char* altrep)
{
   g_return_val_if_fail (value != NULL, list);
   
   ECalComponentText* p = g_new (ECalComponentText, 1);
   p->value = g_strdup (value);
   p->altrep = altrep ? g_strdup (altrep) : NULL;
   return g_slist_append (list, p);
}

void
e_cal_glue_free_calcomponenttext_gslist (GSList* list)
{
   GSList* l = NULL;
   
   for (l = list; l; l = l->next)
      if (l->data) {
         ECalComponentText* p = (ECalComponentText*) l->data;
         
         if (p->value)
            g_free ((char*)p->value);

         if (p->altrep)
            g_free ((char*)p->altrep);
         
         g_free (p);
      }
   
   g_slist_free (list);
}

void
e_cal_glue_free_cal_component (ECalComponent *comp)
{
  g_object_unref (comp);
}

char*
e_cal_glue_cal_component_get_summary (ECalComponent *comp)
{
        ECalComponentText *summary = g_new0 (ECalComponentText, 1);
	e_cal_component_get_summary (comp, summary);
	return g_strdup (summary->value);
}

#include "e-cal-glue.h"

gboolean
e_cal_glue_ecal_get_changes (ECal* ecal,
			     const char* change_id,
			     GSList** newitems,
			     GSList** updated,
			     GSList** removed,
			     GError** error)
{
  GList* changes = NULL;
  GList* l;
  ECalChange* ccc;
  const char* uid = NULL;
  
  GSList *added = NULL, *modified = NULL, *deleted = NULL;

  if (!ecal)
    return FALSE;

  if (!e_cal_get_changes (ecal, change_id, &changes, error))
    return FALSE;

  if (!changes)
    return FALSE;

  for (l = changes; l; l = l->next) {
    ccc = l->data;

    switch (ccc->type) {
    case E_CAL_CHANGE_ADDED:
      added = g_slist_prepend (added, e_cal_component_clone (ccc->comp));
      break;
    case E_CAL_CHANGE_MODIFIED:
      modified = g_slist_prepend (modified, e_cal_component_clone (ccc->comp));
      break;
    case E_CAL_CHANGE_DELETED:
      e_cal_component_get_uid (ccc->comp, &uid);
      deleted = g_slist_prepend (deleted, g_strdup (uid));
      break;
    }
  }

  e_cal_free_change_list (changes);

  if (added)
    *newitems = g_slist_reverse (added);

  if (modified)
    *updated = g_slist_reverse (modified);

  if (deleted)
    *removed = g_slist_reverse (deleted);

  return TRUE;
}


#ifndef _E_CAL_GLUE_RECUR_UTIL_H_
#define _E_CAL_GLUE_RECUR_UTIL_H_

#include <libical/ical.h>

#include "e-cal-glue-comp.h"

ECalGlueRecurrence * e_cal_recur_from_icalproperty (icalproperty *prop, gboolean exception,
						    icaltimezone *zone, gboolean convert_end_date);

#endif

#ifndef __E_CAL_GLUE_H
#define __E_CAL_GLUE_H	1

#include <string.h>
#include <libecal/e-cal.h>
#include <libecal/e-cal-component.h>
#include <libecal/e-cal-time-util.h>
#include <libical/ical.h>

#include "e-cal-glue-comp.h"
#include "e-cal-glue-recur.h"
#include "e-cal-glue-recur-util.h"

gboolean e_cal_glue_ecal_get_changes (ECal* ecal,
			     const char* change_id,
			     GSList** newitems,
			     GSList** updated,
			     GSList** removed,
			     GError** error);

#endif //__E_CAL_GLUE_H

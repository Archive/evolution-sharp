#include <glib.h>

#include "e-cal-glue-recur.h"

void* e_cal_glue_recurrence_get_handle (ECalGlueRecurrence* comp)
{
  g_return_val_if_fail (comp != NULL, NULL);

  return comp->handle;
}

gint e_cal_glue_recurrence_get_frequency (ECalGlueRecurrence* comp)
{
  g_return_val_if_fail (comp != NULL, -1);

  return comp->freq;
}

gint e_cal_glue_recurrence_get_count (ECalGlueRecurrence* comp)
{
  g_return_val_if_fail (comp != NULL, -1);
  
  return comp->count;
}

time_t e_cal_glue_recurrence_get_enddate (ECalGlueRecurrence* comp)
{
  g_return_val_if_fail (comp != NULL, -1);

  return comp->until;
}

gint e_cal_glue_recurrence_get_week_start_day (ECalGlueRecurrence* comp)
{
  g_return_val_if_fail (comp != NULL, -1);

  return comp->week_start_day;
}

gshort e_cal_glue_recurrence_get_interval (ECalGlueRecurrence* comp)
{
  g_return_val_if_fail (comp != NULL, -1);

  return comp->interval;
}

short* e_cal_glue_recurrence_get_by_second (ECalGlueRecurrence* comp, int* array_len)
{
  g_return_val_if_fail (comp != NULL, NULL);
  g_return_val_if_fail (array_len != NULL, NULL);  
  
  *array_len = ICAL_BY_SECOND_SIZE;
  return comp->by_second;
}

short* e_cal_glue_recurrence_get_by_minute (ECalGlueRecurrence* comp, int* array_len)
{
  g_return_val_if_fail (comp != NULL, NULL);
  g_return_val_if_fail (array_len != NULL, NULL);

  *array_len = ICAL_BY_MINUTE_SIZE;
  return comp->by_minute;
}

short* e_cal_glue_recurrence_get_by_hour (ECalGlueRecurrence* comp, int* array_len)
{
  g_return_val_if_fail (comp != NULL, NULL);
  g_return_val_if_fail (array_len != NULL, NULL);

  *array_len = ICAL_BY_HOUR_SIZE;
  return comp->by_hour;
}

short* e_cal_glue_recurrence_get_by_day (ECalGlueRecurrence* comp, int* array_len)
{
  g_return_val_if_fail (comp != NULL, NULL);
  g_return_val_if_fail (array_len != NULL, NULL);

  *array_len = ICAL_BY_DAY_SIZE;
  return comp->by_day;
}

short* e_cal_glue_recurrence_get_by_month_day (ECalGlueRecurrence* comp, int* array_len)
{
  g_return_val_if_fail (comp != NULL, NULL);
  g_return_val_if_fail (array_len != NULL, NULL);

  *array_len = ICAL_BY_MONTHDAY_SIZE;
  return comp->by_month_day;
}

short* e_cal_glue_recurrence_get_by_year_day (ECalGlueRecurrence* comp, int* array_len)
{
  g_return_val_if_fail (comp != NULL, NULL);
  g_return_val_if_fail (array_len != NULL, NULL);

  *array_len = ICAL_BY_YEARDAY_SIZE;
  return comp->by_year_day;
}

short* e_cal_glue_recurrence_get_by_week_number (ECalGlueRecurrence* comp, int* array_len)
{
  g_return_val_if_fail (comp != NULL, NULL);
  g_return_val_if_fail (array_len != NULL, NULL);

  *array_len = ICAL_BY_WEEKNO_SIZE;
  return comp->by_week_no;
}

short* e_cal_glue_recurrence_get_by_month (ECalGlueRecurrence* comp, int* array_len)
{
  g_return_val_if_fail (comp != NULL, NULL);
  g_return_val_if_fail (array_len != NULL, NULL);

  *array_len = ICAL_BY_MONTH_SIZE;
  return comp->by_month;
}

short* e_cal_glue_recurrence_get_by_set_pos (ECalGlueRecurrence* comp, int* array_len)
{
  g_return_val_if_fail (comp != NULL, NULL);
  g_return_val_if_fail (array_len != NULL, NULL);

  *array_len = ICAL_BY_SETPOS_SIZE;
  return comp->by_set_pos;
}


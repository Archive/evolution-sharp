#ifndef _E_CAL_GLUE_COMPONENT_H_
#define _E_CAL_GLUE_COMPONENT_H_

#include <glib/gmacros.h>
#include <time.h>
#include <glib-object.h>
#include <libecal/e-cal.h>
#include <libical/ical.h>

int  e_cal_component_get_priority2 (ECalComponent* comp);

ECalComponentDateTime* e_cal_alloc_ecalcomponentdatetime ();
void e_cal_free_ecalcomponentdatetime                    (ECalComponentDateTime* dt);

struct icaltimetype* e_cal_alloc_icaltimetype            ();
void e_cal_free_icaltimetype                             (struct icaltimetype* dt);

ECalComponentDateTime* e_cal_alloc_ecalcomponentdatetime ();
void e_cal_free_ecalcomponentdatetime                    (ECalComponentDateTime* dt);

time_t e_cal_icaltimetype_to_timet              (const struct icaltimetype* t, int freemem);
time_t e_cal_ecalcomponentdatetime_to_timet     (ECalComponentDateTime* t, int freemem);

struct icaltimetype* e_cal_timet_to_icaltimetype   (int timet);
void e_cal_glue_free_icaltimetype                  (struct icaltimetype* ical);

ECalComponentDateTime* e_cal_timet_to_ecalcomponentdatetime (int timet);
void e_cal_glue_free_ecalcomponentdatetime                  (ECalComponentDateTime* dt);

GSList* e_cal_glue_add_string_to_glib_string_list   (GSList* list, const char* str);
void e_cal_glue_free_glib_string_list               (GSList* list);

GSList* e_cal_glue_add_calcomponenttext_to_gslist (GSList* list, const char* value, const char* altrep);
void e_cal_glue_free_calcomponenttext_gslist                       (GSList* list);

#endif

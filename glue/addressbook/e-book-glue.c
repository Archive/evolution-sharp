#include <libebook/e-book.h>

gboolean
e_book_glue_ebook_get_changes (EBook* ebook,
			       const char* change_id,
			       GSList** newcontacts,
			       GSList** updated,
			       GSList** removed,
			       GError** error)
{
  GList* changes = NULL;
  GList* l;
  EBookChange* ebc;
  EContact* contact;
  const char* uid = NULL;
  
  GSList *added = NULL, *modified = NULL, *deleted = NULL;

  if (!ebook)
    return FALSE;

  if (!e_book_get_changes (ebook, (char *)change_id, &changes, error))
    return FALSE;

  if (!changes)
    return FALSE;

  for (l = changes; l; l = l->next) {
    contact = NULL;
    ebc = l->data;

    switch (ebc->change_type) {
    case E_BOOK_CHANGE_CARD_ADDED:
      contact = e_contact_duplicate (ebc->contact);
      added = g_slist_prepend (added, contact);
      break;
    case E_BOOK_CHANGE_CARD_MODIFIED:
      contact = e_contact_duplicate (ebc->contact);
      modified = g_slist_prepend (modified, contact);
      break;
    case E_BOOK_CHANGE_CARD_DELETED:
      uid = e_contact_get_const (ebc->contact, E_CONTACT_UID);
      deleted = g_slist_prepend (deleted, g_strdup (uid));
      break;
    }    
  }

  e_book_free_change_list (changes);

  if (added)
    *newcontacts = g_slist_reverse (added);

  if (modified)
    *updated = g_slist_reverse (modified);

  if (deleted)
    *removed = g_slist_reverse (deleted);

  return TRUE;
}

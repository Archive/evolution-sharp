/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Copyright (C) 2006 Novell, Inc.
 *
 * Author: Veerapuram Varadhan (vvaradhan@novell.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _E_CONTACT_PHOTO_GLUE_H_
#define _E_CONTACT_PHOTO_GLUE_H_

#include <libebook/e-contact.h>

EContactPhoto* e_contact_photo_glue_new (EContactPhotoType type);
void e_contact_photo_glue_free (EContactPhoto* photo);
void dump_e_contact_photo_glue (EContactPhoto* photo);

EContactPhotoType e_contact_photo_glue_get_photo_type (EContactPhoto *photo);
int e_contact_photo_glue_get_length (EContactPhoto *photo);
char* e_contact_photo_glue_get_mime_type (EContactPhoto *photo);
char* e_contact_photo_glue_get_data (EContactPhoto *photo);
char* e_contact_photo_glue_get_uri (EContactPhoto *photo);

void e_contact_photo_glue_set_photo_type (EContactPhoto *photo, EContactPhotoType type);
void e_contact_photo_glue_set_length (EContactPhoto *photo, int length);
void e_contact_photo_glue_set_mime_type (EContactPhoto *photo, char* mime_type);
void e_contact_photo_glue_set_data (EContactPhoto *photo, char* data);
void e_contact_photo_glue_set_uri (EContactPhoto *photo, char* uri);

#endif

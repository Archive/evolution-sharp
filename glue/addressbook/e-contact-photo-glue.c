/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Copyright (C) 2006 Novell, Inc.
 *
 * Author: Veerapuram Varadhan (vvaradhan@novell.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include "e-contact-photo-glue.h"

EContactPhoto*
e_contact_photo_glue_new (EContactPhotoType type)
{
	EContactPhoto *photo = g_new0 (EContactPhoto, 1);

	photo->type = type;
	return photo;
}

EContactPhotoType 
e_contact_photo_glue_get_photo_type (EContactPhoto *photo)
{
	if (!photo)
		return -1;

	return photo->type;
}

int 
e_contact_photo_glue_get_length (EContactPhoto *photo)
{
	if (!photo)
		return -1;

	if (photo->type == E_CONTACT_PHOTO_TYPE_INLINED)
		return photo->data.inlined.length;

	return -1;
}

char* 
e_contact_photo_glue_get_mime_type (EContactPhoto *photo)
{
	if (!photo)
		return NULL;

	if (photo->type == E_CONTACT_PHOTO_TYPE_INLINED)
		return photo->data.inlined.mime_type;
	
	return NULL;
}

char*
e_contact_photo_glue_get_data (EContactPhoto *photo)
{
	if (!photo)
		return NULL;
	if (photo->type == E_CONTACT_PHOTO_TYPE_INLINED)
		return photo->data.inlined.data;
	
	return NULL;
}

char* 
e_contact_photo_glue_get_uri (EContactPhoto *photo)
{
	if (!photo)
		return NULL;
	if (photo->type == E_CONTACT_PHOTO_TYPE_URI)
		return photo->data.uri;
	
	return NULL;
}

void 
e_contact_photo_glue_set_photo_type (EContactPhoto *photo, EContactPhotoType type)
{
	if (!photo)
		return;

	photo->type = type;
}

void 
e_contact_photo_glue_set_length (EContactPhoto *photo, int length)
{
	if (!photo)
		return;
	
	if (photo->type == E_CONTACT_PHOTO_TYPE_INLINED)
		photo->data.inlined.length = length;
}

void 
e_contact_photo_glue_set_mime_type (EContactPhoto *photo, char* mime_type)
{
	if (!photo)
		return;

	if (photo->type == E_CONTACT_PHOTO_TYPE_INLINED) {
		g_free (photo->data.inlined.mime_type);
		photo->data.inlined.mime_type = g_strdup (mime_type);
	}
}

void 
e_contact_photo_glue_set_data (EContactPhoto *photo, char* data)
{
	if (!photo)
		return;

	if (photo->type == E_CONTACT_PHOTO_TYPE_INLINED) {
		g_free (photo->data.inlined.data);
		photo->data.inlined.data = g_memdup (data, photo->data.inlined.length);
	}
}

void 
e_contact_photo_glue_set_uri (EContactPhoto *photo, char* uri)
{
	if (!photo)
		return;

	if (photo->type == E_CONTACT_PHOTO_TYPE_URI) {
		g_free (photo->data.uri);
		photo->data.uri = g_strdup (uri);
	}
}

void
dump_e_contact_photo_glue (EContactPhoto* photo)
{
	if (!photo) {
		g_print ("Photo is null\n");
		return;
	}

	g_print ("*********** Dumping e_contact_photo ****************\n");

	if (photo->type == E_CONTACT_PHOTO_TYPE_URI)
		g_print ("Uri: [%s]\n", photo->data.uri);
	else {
		g_print ("Length: [%d]\n", photo->data.inlined.length);
		g_print ("Data: [%p]\n", photo->data.inlined.data);
		g_print ("Mimetype: [%s]\n", photo->data.inlined.mime_type);
	}
	g_print ("********** Dumping e_contact_photo ends *************\n");
	
}

void
e_contact_photo_glue_free (EContactPhoto* photo)
{
	if (!photo)
		return;

	if (photo->type == E_CONTACT_PHOTO_TYPE_INLINED) {
		g_free (photo->data.inlined.data);
		g_free (photo->data.inlined.mime_type);
	} else
		g_free (photo->data.uri);

	g_free (photo);
}

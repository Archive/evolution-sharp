%.h: %.list
	( $(GLIB_GENMARSHAL) --prefix=$(subst -,_,$(subst $(GLUE_SRC_DIR)/,,$*)) $(srcdir)/$< --header > $@.tmp \
	&& mv $@.tmp $@ ) || ( rm -f $@.tmp && exit 1 )

%.c: %.list %.h
	( (echo "#include \"$(subst $(GLUE_SRC_DIR)/,,$*).h\""; $(GLIB_GENMARSHAL) --prefix=$(subst -,_,$(subst $(GLUE_SRC_DIR)/,,$*)) $(srcdir)/$*.list --body) > $@.tmp \
	&& mv $@.tmp $@ ) || ( rm -f $@.tmp && exit 1 )

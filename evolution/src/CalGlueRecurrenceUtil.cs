using System;
using System.Runtime.InteropServices;

namespace Evolution {
	public class CalGlueRecurrenceUtil {

		[DllImport("evolutionglue")]
		static extern int e_cal_glue_recurrence_get_frequency(IntPtr raw);
		public static FrequencyType GetFrequency (IntPtr Handle)
		{
			int raw_ret = e_cal_glue_recurrence_get_frequency (Handle);
			return (FrequencyType)raw_ret;
		}

		[DllImport("evolutionglue")]
		static extern int e_cal_glue_recurrence_get_count (IntPtr raw);
		public static int GetCount (IntPtr Handle)
		{
			int raw_ret = e_cal_glue_recurrence_get_count (Handle);
			return raw_ret;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_enddate(IntPtr raw);

		public static DateTime GetEnddate (IntPtr Handle)
		{
			IntPtr raw_ret = e_cal_glue_recurrence_get_enddate (Handle);
			DateTime ret;
			
			//
			// If raw_ret is zero, we will return DateTime.MinValue.
			//
			if (raw_ret == IntPtr.Zero)
				ret = DateTime.MinValue;
			else 
				ret = GLib.Marshaller.time_tToDateTime (raw_ret);
			
			return ret;
		}

		[DllImport("evolutionglue")]
		static extern int e_cal_glue_recurrence_get_week_start_day (IntPtr raw);

		public static int GetWeekStartDay (IntPtr Handle)
		{ 
			int raw_ret = e_cal_glue_recurrence_get_week_start_day (Handle);
			return raw_ret;
		}

		[DllImport("evolutionglue")]
		static extern short e_cal_glue_recurrence_get_interval (IntPtr raw);

		public static short GetInterval (IntPtr Handle)
		{ 
			short raw_ret = e_cal_glue_recurrence_get_interval (Handle);
			return raw_ret;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_by_second (IntPtr raw, ref int length);

		public static short[] GetBySecond (IntPtr Handle)
		{ 
			int length = 0;
			short[] ret = null;

			IntPtr raw_ret = e_cal_glue_recurrence_get_by_second(Handle, ref length);

			if (raw_ret != IntPtr.Zero) {
				ret = new short [length];
				Marshal.Copy (raw_ret, ret, 0, length);
			}
			return ret;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_by_minute(IntPtr raw, ref int length);

		public static short[] GetByMinute (IntPtr Handle) 
		{
			int length = 0;
			short[] ret = null;

			IntPtr raw_ret = e_cal_glue_recurrence_get_by_minute(Handle, ref length);

			if (raw_ret != IntPtr.Zero) {
				ret = new short [length];
				Marshal.Copy (raw_ret, ret, 0, length);
			}
			return ret;

		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_by_hour(IntPtr raw, ref int length);

		public static short[] GetByHour (IntPtr Handle)
                {
                        int length = 0;
                        short[] ret = null;

			IntPtr raw_ret = e_cal_glue_recurrence_get_by_hour(Handle, ref length);

			if (raw_ret != IntPtr.Zero) {
				ret = new short [length];
				Marshal.Copy (raw_ret, ret, 0, length);
			}
                        return ret;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_by_day(IntPtr raw, ref int length);

		public static short[] GetByDay (IntPtr Handle)
                {
                        int length = 0;
                        short[] ret = null;

			IntPtr raw_ret = e_cal_glue_recurrence_get_by_day(Handle, ref length);

                        if (raw_ret != IntPtr.Zero) {
				ret = new short [length];
				Marshal.Copy (raw_ret, ret, 0, length);
			}
                        return ret;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_by_month_day(IntPtr raw, ref int length);

		public static short[] GetByMonthDay (IntPtr Handle)
                {
                        int length = 0;
                        short[] ret = null;

			IntPtr raw_ret = e_cal_glue_recurrence_get_by_month_day(Handle, ref length);

                        if (raw_ret != IntPtr.Zero) {
				ret = new short [length];
				Marshal.Copy (raw_ret, ret, 0, length);
			}
                        return ret;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_by_year_day(IntPtr raw, ref int length);

		public static short[] GetByYearDay (IntPtr Handle)
		{ 
			int length = 0;
			short[] ret = null;
			
			IntPtr raw_ret = e_cal_glue_recurrence_get_by_year_day(Handle, ref length);
			
			if (raw_ret != IntPtr.Zero) {
				ret = new short [length];
				Marshal.Copy (raw_ret, ret, 0, length);
			}
			return ret;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_by_week_number(IntPtr raw, ref int length);

		public static short[] GetByWeekNumber (IntPtr Handle)
		{ 
                        int length = 0;
                        short[] ret = null;

			IntPtr raw_ret = e_cal_glue_recurrence_get_by_week_number(Handle, ref length);

                        if (raw_ret != IntPtr.Zero) {
				ret = new short [length];
				Marshal.Copy (raw_ret, ret, 0, length);
			}
                        return ret;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_by_month(IntPtr raw, ref int length);

		public static short[] GetByMonth (IntPtr Handle)
		{ 
                        int length = 0;
                        short[] ret = null;

			IntPtr raw_ret = e_cal_glue_recurrence_get_by_month(Handle, ref length);

                        if (raw_ret != IntPtr.Zero) {
				ret = new short [length];
				Marshal.Copy (raw_ret, ret, 0, length);
			}
                        return ret;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_glue_recurrence_get_by_set_pos(IntPtr raw, ref int length);

		public static short[] GetBySetPos (IntPtr Handle)
		{ 
                        int length = 0;
                        short[] ret = null;

			IntPtr raw_ret = e_cal_glue_recurrence_get_by_set_pos(Handle, ref length);

                        if (raw_ret != IntPtr.Zero) {
				ret = new short [length];
				Marshal.Copy (raw_ret, ret, 0, length);
			}
                        return ret;
		}
	}
}

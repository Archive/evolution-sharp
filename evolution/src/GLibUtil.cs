using System;
using System.Runtime.InteropServices;

namespace Evolution {
	public class GLibUtil {
		public static DateTime local_epoch = new DateTime (1970, 1, 1);

		public static int DateTimeToUnixtime_t (DateTime time)
		{
			return ((int)time.Subtract (local_epoch).TotalSeconds);
		}

		public static DateTime Unixtime_tToDateTime (int timet)
		{
			if (timet < 0)
				return DateTime.MinValue;
         
			DateTime t = local_epoch;
			return t.AddSeconds (timet);
		}
	
		[DllImport("libglib-2.0.so.0")]
		public static extern int g_slist_length (IntPtr slist);
		
		[DllImport("glibsharpglue-2")]
		public static extern IntPtr gtksharp_slist_get_data (IntPtr slist);

		[DllImport("glibsharpglue-2")]
		public static extern  IntPtr gtksharp_slist_get_next (IntPtr slist);
 
		[DllImport("libglib-2.0.so.0")]
		public static extern void g_slist_free (IntPtr slist);

		[DllImport("ecal")]
		public static extern void e_cal_component_free_text_list (IntPtr raw);
      
		public static string[] GSListToStringArray (IntPtr slist, bool free_list_mem)
		{
			string[] ret;
			int i = 0;
			IntPtr iter = IntPtr.Zero;
			
			ret = new string [g_slist_length (slist)];
	
			//
			// The unmanaged-pointers are freed by the caller
			//
			for (i = 0, iter=slist; iter != IntPtr.Zero; iter = gtksharp_slist_get_next (iter), i++) {
				if (!free_list_mem)
					ret[i] = Marshal.PtrToStringAnsi (gtksharp_slist_get_data (iter));
				else
					ret[i] = GLib.Marshaller.PtrToStringGFree (gtksharp_slist_get_data (iter));
			}
			if (free_list_mem)
				g_slist_free (slist);
			return ret;
		}		

		public static string[] CalComponentTextGSListToStringArray (IntPtr slist)
		{
			string[] ret;
			int i = 0;
			IntPtr iter = IntPtr.Zero;
			
			ret = new string [g_slist_length (slist)];
	
			for (i = 0, iter=slist; iter != IntPtr.Zero; iter = gtksharp_slist_get_next (iter), i++) {
				IntPtr data = gtksharp_slist_get_data (iter);
            
				if (data == IntPtr.Zero)
					continue;
            
				Evolution.CalComponentText cct =
					(CalComponentText) Marshal.PtrToStructure (data, typeof (CalComponentText));
            
				ret[i] = cct.value;
			}

			return ret;
		}		
      
		[DllImport("evolutionglue")]
		public static extern IntPtr e_cal_glue_add_string_to_glib_string_list (IntPtr list, string str);
		[DllImport("evolutionglue")]
		public static extern void e_cal_glue_free_glib_string_list (IntPtr list);

		public static IntPtr StringArrayToGSList (string[] list)
		{
			IntPtr glist = IntPtr.Zero;
         
			foreach (string s in list)
				glist = e_cal_glue_add_string_to_glib_string_list (glist, s);
            
			return glist;
		}
      
		public static void FreeGSList (IntPtr list)
		{
			e_cal_glue_free_glib_string_list (list);
		}
      

		[DllImport("evolutionglue")]
		public static extern IntPtr e_cal_glue_add_calcomponenttext_to_gslist (IntPtr list, string str, string altrep);
		[DllImport("evolutionglue")]
		public static extern void e_cal_glue_free_calcomponenttext_gslist (IntPtr list);

		public static IntPtr CalComponentTextArrayToGSList (CalComponentText[] list)
		{
			IntPtr glist = IntPtr.Zero;
         
			foreach (CalComponentText s in list)
				glist = e_cal_glue_add_calcomponenttext_to_gslist (glist, s.value, s.altrep);
            
			return glist;
		}
      
		public static void FreeCalComponentTextGSList (IntPtr list)
		{
			e_cal_glue_free_calcomponenttext_gslist (list);
		}

		[DllImport("libglib-2.0.so.0")]
		public static extern IntPtr g_slist_append (IntPtr list, IntPtr val);
      
		public static IntPtr CalComponentAttendeeArrayToGSList (CalComponentAttendee[] list)
		{
			IntPtr ptr = IntPtr.Zero;
			IntPtr glist = IntPtr.Zero;
         
			foreach (CalComponentAttendee a in list) {
				ptr = Marshal.AllocHGlobal(Marshal.SizeOf(a));
				Marshal.StructureToPtr (a, ptr, false);
				glist = g_slist_append (glist, ptr);
			}
         
			return glist;
		}
      
		public static void FreeCalComponentAttendeeGSList (IntPtr list)
		{
			g_slist_free (list);
		}
	}
}

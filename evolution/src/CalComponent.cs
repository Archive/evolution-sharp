using System;
using System.Collections;
using System.Runtime.InteropServices;      
	
namespace Evolution {

	public enum CalClassification : int {
		None,
		Public,
		Private,
		Confidental,
		}

#if USE_EDS_2_24
	public enum CalTransparency : int {
		None,
		Transparent,
		Opaque,
		Unknown,
	}
#endif	

	public enum CalPriority : int {
		Undefined   = 0,
		High        = 3,
		Normal      = 5,
		Low         = 7
		}
	
	public enum CalStatus : int {
		Undefined   = 0,
		None        = 10060,
		Final       = 10059,
		Draft       = 10058,
		InProcess   = 10057,
		Cancelled   = 10056,
		NeedsAction = 10055,
		Completed   = 10054,
		Confirmed   = 10053,
		Tentative   = 10052,
		X           = 10051
		}
   
	public enum CalObjModType {
		This           = 1 << 0,
		ThisAndPrior   = 1 << 1,
		ThisAndFuture  = 1 << 2,
		All            = 0x07
	}
   
	public enum CalComponentVType {
		Type,
		Event,
		Todo,
		Journal,
		FreeBusy,
		TimeZone
	}

#if USE_EDS_2_24
	public enum CalComponentRangeType {
		Single,
		ThisPrior,
		ThisFuture		
	}

	public struct CalComponentRange {
		public CalComponentRangeType rangeType;
		public CalComponentDateTime dt;
	}

#endif	
   
	public struct CalComponentText {
		public string value;
		public string altrep;
      
		public CalComponentText (string _value, string _altrep)
		{
			value = _value;
			altrep = _altrep;
		}
	}
   
	public enum icalparameter_cutype {
		X = 20004,
		Individual = 20005,
		Group = 20006,
		Resource = 20007,
		Room = 20008,
		Unknown = 20009,
		None = 20010
	}
   
	public enum icalparameter_role {
		X = 20051,
		Chair = 20052,
		ReqParticipant = 20053,
		OptParticipant = 20054,
		NonParticipant = 20055,
		None = 20056
	}
   
	public enum icalparameter_partstat {
		X = 20029,
		NeedsAction = 20030,
		Accepted = 20031,
		Declined = 20032,
		Tentative = 20033,
		Delegated = 20034,
		Completed = 20035,
		InProcess = 20036,
		None = 20037
	}                                                    
   
	[StructLayout(LayoutKind.Sequential)]
	public struct CalComponentAttendee {
		public string value;
		public string member;
		public icalparameter_cutype cutype;
		public icalparameter_role role;
		public icalparameter_partstat status;
		public bool rsvp;
		public string delto;
		public string delfrom;
		public string sentby;
		public string cn;
		public string language;
	}
	
	[StructLayout(LayoutKind.Sequential)]
	public struct CalComponentOrganizer {
		public string Value;
		public string sentby;
		public string cn;
		public string language;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct CalComponentId {
		public string uid;
		public string rid;
	}
	
	public class CalComponent {

		bool new_component;
		IntPtr handle;
		Cal ecal;

		[DllImport("ecal")]
			static extern IntPtr e_cal_component_new ();
		[DllImport("ecal")]
			static extern void e_cal_component_set_new_vtype (IntPtr handle, CalComponentVType type);
		[DllImport("ecal")]
			static extern IntPtr e_cal_component_new_from_string (string calobj);

		public CalComponent ()
		{
			new_component = true;
			handle = e_cal_component_new ();
			ecal = null;
		}
		
		public CalComponent (string calobj)
		{
			new_component = true;
			handle = e_cal_component_new_from_string (calobj);
			ecal = null;
		}

		public CalComponent (Cal _ecal)
		{
			new_component = true;
			handle = e_cal_component_new ();
			ecal = _ecal;
         
			switch (ecal.SourceType) {
			case CalSourceType.Event:
				e_cal_component_set_new_vtype (handle, CalComponentVType.Event);
				break;
			case CalSourceType.Todo:
				e_cal_component_set_new_vtype (handle, CalComponentVType.Todo);
				break;
			case CalSourceType.Journal:
				e_cal_component_set_new_vtype (handle, CalComponentVType.Journal);
				break;
			}
		}
		
		public CalComponent (CalComponentVType stype)
		{
			new_component = true;
			handle = e_cal_component_new ();
			e_cal_component_set_new_vtype (handle, stype);
		}

		public CalComponent (IntPtr _handle)
		{
			new_component = false;
			handle = _handle;
			ecal = null;
		}

		public CalComponent (IntPtr _handle, Cal _ecal)
		{
			new_component = false;
			handle = _handle;
			ecal = _ecal;
		}

		public IntPtr Handle {
			get {
				return handle;
			}
		}

		public Cal ECal {
			get {
				return ecal;
			}
			set {
				ecal = value;
			}
		}

		public Evolution.Source Source {
			get {
				if (ecal != null)
					return ecal.Source;
				else
					return null;
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_uid (IntPtr handle, out IntPtr uid);
		[DllImport("ecal")]
			static extern void e_cal_component_set_uid (IntPtr handle, string uid);
		
		public string Uid {
			get {
				IntPtr uid;
			    	e_cal_component_get_uid (Handle, out uid);
			    	string managed_uid = Marshal.PtrToStringAnsi (uid);

			    	return managed_uid;
			}
			set {
				e_cal_component_set_uid (Handle, value);
			}
		}
		
		[DllImport("ecal")]
			static extern CalComponentVType e_cal_component_get_vtype (IntPtr handle);

		public CalComponentVType VType 
		{
			get
			{
				return e_cal_component_get_vtype (Handle);
			}
		}

		[DllImport ("ecal")]
			static extern IntPtr e_cal_component_get_id (IntPtr handle);
		[DllImport ("ecal")]
			static extern void e_cal_component_free_id (IntPtr handle);

		public CalComponentId Id {
			get {
				IntPtr id;
				CalComponentId cid;

				id = e_cal_component_get_id (Handle);
				cid = (CalComponentId) Marshal.PtrToStructure (id, typeof (CalComponentId));
				e_cal_component_free_id (id);

				return cid;
			}
		}


		[DllImport("ecal")]
			static extern string e_cal_component_get_recurid_as_string (IntPtr handle);
		
		public string RecurId {
			get {
				return e_cal_component_get_recurid_as_string (Handle);
			}
		}

#if USE_EDS_2_24
		[DllImport("ecal")]
			static extern void e_cal_component_get_recurid (IntPtr handle, out IntPtr recurid);
		[DllImport("ecal")]
			static extern void e_cal_component_set_recurid (IntPtr handle, IntPtr recurid);

		public DateTime Rid {
			get 
			{
				IntPtr native_r = IntPtr.Zero;
				e_cal_component_get_recurid (Handle, out native_r);
				CalComponentRange range = (CalComponentRange) Marshal.PtrToStructure (native_r, 
						typeof (CalComponentRange));

				CalComponentDateTime cdt = range.dt;
				Icaltimetype icalt = cdt.IcalTime;
				DateTime dt = icalt.AsDateTimeUtc;

				Marshal.FreeHGlobal (native_r);
				return dt;
			}

			set
			{
				Icaltimetype icalt = new Icaltimetype ();
				icalt.SetDateTime = value;

				CalComponentDateTime cdt = new CalComponentDateTime ();
				cdt.IcalTime = icalt;
				cdt.Tzid = "UTC";
				
				CalComponentRange range = new CalComponentRange ();
				range.rangeType = CalComponentRangeType.Single;
				range.dt = cdt;

				IntPtr raw = Marshal.AllocHGlobal (Marshal.SizeOf (range));
				Marshal.StructureToPtr (range, raw, false);

				e_cal_component_set_recurid (Handle, raw);
				Marshal.FreeHGlobal (raw);
			}
		}
#endif

		[DllImport("ecal")]
			static extern void e_cal_component_get_summary (IntPtr raw, ref CalComponentText text);
		[DllImport("ecal")]
			static extern void e_cal_component_set_summary (IntPtr raw, ref CalComponentText text);
		[DllImport("evolutionglue")]
		        static extern string e_cal_glue_cal_component_get_summary (IntPtr comp);

		public string Summary {
			get {
			        string temp = e_cal_glue_cal_component_get_summary (handle);
				return temp;
			}
			set {
				CalComponentText text = new CalComponentText (value, null);
				e_cal_component_set_summary (Handle, ref text);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_status (IntPtr raw, out int status);
		[DllImport("ecal")]
			static extern void e_cal_component_set_status (IntPtr raw, int status);

		public CalStatus Status {
			get {
				int status = 0;
				e_cal_component_get_status (Handle, out status);
				return (CalStatus) status;
			}
			set {
				e_cal_component_set_status (Handle, (int) value);
			}
		}
		
		[DllImport("ecal")]
			static extern int e_cal_component_get_percent_as_int (IntPtr raw);
		[DllImport("ecal")]
			static extern void e_cal_component_set_percent_as_int (IntPtr raw, int percent);

		public int Percent
		{
			get
			{
				return e_cal_component_get_percent_as_int (Handle);
			}
			set
			{
				e_cal_component_set_percent_as_int (Handle, value);
			}
		}

		[DllImport("evolutionglue")]
			static extern int e_cal_component_get_priority2 (IntPtr raw);
		[DllImport("ecal")]
			static extern int e_cal_component_set_priority (IntPtr raw, ref int priority);

		public CalPriority Priority {
			get {
				return (CalPriority) e_cal_component_get_priority2 (Handle);
			}
			set {
				int priority = (int) value;
				e_cal_component_set_priority (Handle, ref priority);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_classification (IntPtr raw, ref CalClassification cl );
		[DllImport("ecal")]
			static extern void e_cal_component_set_classification (IntPtr raw, CalClassification cl );

		public CalClassification Classification {
			get {
				CalClassification cl = CalClassification.None;
				e_cal_component_get_classification (Handle, ref cl);
				return cl;
			}
			set {
				e_cal_component_set_classification (Handle, value);
			}
		}

#if USE_EDS_2_24
		[DllImport("ecal")]
			static extern void e_cal_component_get_transparency (IntPtr raw, ref CalTransparency trans);
		[DllImport("ecal")]
			static extern void e_cal_component_set_transparency (IntPtr raw, CalTransparency trans);

		public CalTransparency Transparency {
			get
			{
				CalTransparency tr = CalTransparency.None;
				e_cal_component_get_transparency (Handle, ref tr);
				return tr;
			}
			set 
			{
				e_cal_component_set_transparency (Handle, value);
			}
		}
#endif

		[DllImport("ecal")]
			static extern void e_cal_component_get_location (IntPtr raw, out IntPtr location);
		[DllImport("ecal")]
			static extern void e_cal_component_set_location (IntPtr raw, string location);

		public string Location {
			get {
				IntPtr location;
				e_cal_component_get_location (Handle, out location);
				string managed_location = Marshal.PtrToStringAnsi (location);

				return managed_location;
			}
			set {
				e_cal_component_set_location (Handle, value);
			}
		}

#if USE_EDS_2_24
		[DllImport("ecal")]
			static extern bool e_cal_component_is_allday (IntPtr raw);

		public bool IsAllDay
		{
			get
			{
				bool ret = e_cal_component_is_allday (Handle);

				return ret;
			}
		}
#endif

		[DllImport("ecal")]
			static extern void e_cal_component_get_dtstart (IntPtr raw, IntPtr dt);
		[DllImport("ecal")]
			static extern void e_cal_component_set_dtstart (IntPtr raw, IntPtr dt);
      
		[DllImport("ecalglue")]
			static extern void e_cal_set_icaltimetype_to_ecalcomponentdatetime (IntPtr raw, IntPtr type);

		public DateTime Dtstart {
			get {
				IntPtr dt = CalUtil.e_cal_alloc_ecalcomponentdatetime ();
				e_cal_component_get_dtstart (Handle, dt);
				DateTime ret = CalUtil.ecalcomponentdatetime_to_datetime (dt);
				CalUtil.e_cal_free_ecalcomponentdatetime (dt);
				return ret;
			}
			set {
				IntPtr dt = CalUtil.datetime_to_ecalcomponentdatetime (value);
				e_cal_component_set_dtstart (Handle, dt);
				CalUtil.e_cal_glue_free_ecalcomponentdatetime (dt);
			}
		} 

#if USE_EDS_2_24
		public CalComponentDateTime DtStart {
			get {
				CalComponentDateTime dt = new CalComponentDateTime ();
				IntPtr native_dt = GLib.Marshaller.StructureToPtrAlloc (dt);
				e_cal_component_get_dtstart(Handle, native_dt);
				dt = (CalComponentDateTime) Marshal.PtrToStructure (native_dt, typeof (CalComponentDateTime));
				Marshal.FreeHGlobal (native_dt);
				
				return dt;
			}
			set {	
				IntPtr native_dt = GLib.Marshaller.StructureToPtrAlloc (value);
				e_cal_component_set_dtstart(Handle, native_dt);
				Marshal.FreeHGlobal (native_dt);
			}
		}

		public CalComponentDateTime DtEnd {
			get {
				CalComponentDateTime dt = new CalComponentDateTime ();
				IntPtr native_dt = GLib.Marshaller.StructureToPtrAlloc (dt);
				e_cal_component_get_dtend(Handle, native_dt);
				dt = (CalComponentDateTime) Marshal.PtrToStructure (native_dt, typeof (CalComponentDateTime));
				Marshal.FreeHGlobal (native_dt);
				
				return dt;
			}
			set {	
				IntPtr native_dt = GLib.Marshaller.StructureToPtrAlloc (value);
				e_cal_component_set_dtend(Handle, native_dt);
				Marshal.FreeHGlobal (native_dt);
			}
		}
		
		public CalComponentDateTime DueDate {
			get {
				CalComponentDateTime dt = new CalComponentDateTime ();
				IntPtr native_dt = GLib.Marshaller.StructureToPtrAlloc (dt);
				e_cal_component_get_due (Handle, native_dt);
				dt = (CalComponentDateTime) Marshal.PtrToStructure (native_dt, typeof (CalComponentDateTime));
				Marshal.FreeHGlobal (native_dt);
				
				return dt;
			}
			set {	
				IntPtr native_dt = GLib.Marshaller.StructureToPtrAlloc (value);
				e_cal_component_set_due (Handle, native_dt);
				Marshal.FreeHGlobal (native_dt);
			}
		}


#endif		

		[DllImport("ecal")]
			static extern void e_cal_component_get_dtend (IntPtr raw, IntPtr dt);
		[DllImport("ecal")]
			static extern void e_cal_component_set_dtend (IntPtr raw, IntPtr dt);

		public DateTime Dtend {
			get {
				IntPtr dt = CalUtil.e_cal_alloc_ecalcomponentdatetime ();
				e_cal_component_get_dtend (Handle, dt);
				DateTime ret = CalUtil.ecalcomponentdatetime_to_datetime (dt);
				CalUtil.e_cal_free_ecalcomponentdatetime (dt);
				return ret;
			}
			set {
				IntPtr dt = CalUtil.datetime_to_ecalcomponentdatetime (value);
				e_cal_component_set_dtend (Handle, dt);
				CalUtil.e_cal_glue_free_ecalcomponentdatetime (dt);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_dtstamp (IntPtr raw, IntPtr dt);
		[DllImport("ecal")]
			static extern void e_cal_component_set_dtstamp (IntPtr raw, IntPtr dt);

		public DateTime Dtstamp {
			get {
				IntPtr dt = CalUtil.e_cal_alloc_icaltimetype ();
				e_cal_component_get_dtstamp (Handle, dt);
				DateTime ret = CalUtil.icaltimetype_to_datetime (dt, false);
				CalUtil.e_cal_free_icaltimetype (dt);
				return ret;
			}
			set {
				IntPtr dt = CalUtil.datetime_to_icaltimetype (value);
				e_cal_component_set_dtstamp (Handle, dt);
				CalUtil.e_cal_glue_free_icaltimetype (dt);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_due (IntPtr raw, IntPtr dt);
		[DllImport("ecal")]
			static extern void e_cal_component_set_due (IntPtr raw, IntPtr dt);

		public DateTime Due {
			get {
				IntPtr dt = CalUtil.e_cal_alloc_ecalcomponentdatetime ();
				e_cal_component_get_due (Handle, dt);
				DateTime ret = CalUtil.ecalcomponentdatetime_to_datetime (dt);
				CalUtil.e_cal_free_ecalcomponentdatetime (dt);
				return ret;
			}
			set {
				IntPtr dt = CalUtil.datetime_to_ecalcomponentdatetime (value);
				e_cal_component_set_due (Handle, dt);
				CalUtil.e_cal_glue_free_ecalcomponentdatetime (dt);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_last_modified (IntPtr raw, out IntPtr dt);
		[DllImport("ecal")]
			static extern void e_cal_component_set_last_modified (IntPtr raw, IntPtr dt);

		public DateTime LastModified {
			get {
				IntPtr dt = IntPtr.Zero;
				e_cal_component_get_last_modified (Handle, out dt);
				return CalUtil.icaltimetype_to_datetime (dt, true);
			}
			set
			{
				IntPtr dt = CalUtil.datetime_to_icaltimetype (value);
				e_cal_component_set_last_modified (Handle, dt);
				CalUtil.e_cal_glue_free_icaltimetype (dt);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_created (IntPtr raw, out IntPtr dt);
		[DllImport("ecal")]
			static extern void e_cal_component_set_created (IntPtr raw, IntPtr dt);

		public DateTime Created {
			get {
				IntPtr dt = IntPtr.Zero;
				e_cal_component_get_created (Handle, out dt);
				return CalUtil.icaltimetype_to_datetime (dt, true);
			}
			set {
				IntPtr dt = CalUtil.datetime_to_icaltimetype (value);
				e_cal_component_set_created (Handle, dt);
				CalUtil.e_cal_glue_free_icaltimetype (dt);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_completed (IntPtr raw, out IntPtr dt);
		[DllImport("ecal")]
			static extern void e_cal_component_set_completed (IntPtr raw, IntPtr dt);

		public DateTime Completed {
			get {
				IntPtr dt = IntPtr.Zero;
				e_cal_component_get_completed (Handle, out dt);
				return CalUtil.icaltimetype_to_datetime (dt, true);
			}
			set {
				IntPtr dt = CalUtil.datetime_to_icaltimetype (value);
				e_cal_component_set_completed (Handle, dt);
				CalUtil.e_cal_glue_free_icaltimetype (dt);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_attendee_list (IntPtr raw, out IntPtr att);
		[DllImport("ecal")]
			static extern void e_cal_component_set_attendee_list (IntPtr raw, IntPtr att);
		[DllImport("ecal")]
			static extern bool e_cal_component_has_attendees (IntPtr raw);
		[DllImport("ecal")]
			static extern void e_cal_component_free_attendee_list (IntPtr raw_ret);

		public CalComponentAttendee[] Attendees {
			get {
				IntPtr raw_ret = IntPtr.Zero;
				IntPtr iter = IntPtr.Zero;
				int i = 0;
				int len = 0;
				CalComponentAttendee[] att_ret = null;
            
				if (e_cal_component_has_attendees (Handle)) {
					e_cal_component_get_attendee_list (Handle, out raw_ret);
               
					if (raw_ret != IntPtr.Zero) {
						if ((len = GLibUtil.g_slist_length (raw_ret)) > 0 ) {
							att_ret = new CalComponentAttendee [len];

							for (i = 0, iter=raw_ret; iter != IntPtr.Zero; 
							     iter = GLibUtil.gtksharp_slist_get_next (iter), i++) {
								IntPtr data = GLibUtil.gtksharp_slist_get_data (iter);
								CalComponentAttendee a = (CalComponentAttendee) Marshal.PtrToStructure (data, typeof (CalComponentAttendee));
								att_ret [i] = a;
							}
						}
                  
						e_cal_component_free_attendee_list (raw_ret);
					}
				}
            
				return att_ret == null ? new CalComponentAttendee [0] : att_ret;
			}
			set {
				IntPtr raw = GLibUtil.CalComponentAttendeeArrayToGSList (value);
				e_cal_component_set_attendee_list (Handle, raw);
				GLibUtil.FreeCalComponentAttendeeGSList (raw);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_organizer (IntPtr raw, ref CalComponentOrganizer org);
		[DllImport("ecal")]
			static extern void e_cal_component_set_organizer (IntPtr raw, ref CalComponentOrganizer org);
		
		public CalComponentOrganizer Organizer {
			get 
			{
				CalComponentOrganizer org = new CalComponentOrganizer ();
				e_cal_component_get_organizer (Handle, ref org);
				return org;
			}
			set
			{
				e_cal_component_set_organizer (Handle, ref value);
			}
		}


		[DllImport("ecal")]
			static extern void e_cal_component_get_description_list (IntPtr raw, out IntPtr list);
		[DllImport("ecal")]
			static extern void e_cal_component_set_description_list (IntPtr raw, IntPtr list);

		public string[] Descriptions {
			get {
				IntPtr raw_ret = IntPtr.Zero;
				e_cal_component_get_description_list(Handle, out raw_ret);
				string[] ret = CalUtil.CalComponentTextGSListToStringArray (raw_ret);
				CalUtil.e_cal_component_free_text_list (raw_ret);
				return ret;
			}
			set {
				CalComponentText[] val = new CalComponentText [value.Length];
            
				for (int i = 0; i < value.Length; i++) {
					val [i].value = value [i];
					val [i].altrep = "";
				}
            
				IntPtr list = GLibUtil.CalComponentTextArrayToGSList (val);
				e_cal_component_set_description_list (Handle, list);
				GLibUtil.FreeCalComponentTextGSList (list);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_comment_list (IntPtr raw, out IntPtr list);
		[DllImport("ecal")]
			static extern void e_cal_component_set_comment_list (IntPtr raw, IntPtr list);

		public string[] Comments {
			get {
				IntPtr raw_ret = IntPtr.Zero;
				e_cal_component_get_comment_list(Handle, out raw_ret);
				string[] ret = CalUtil.CalComponentTextGSListToStringArray (raw_ret);
				CalUtil.e_cal_component_free_text_list (raw_ret);
				return ret;
			}
			set {
				CalComponentText[] val = new CalComponentText [value.Length];
            
				for (int i = 0; i < value.Length; i++) {
					val [i].value = value [i];
					val [i].altrep = "";
				}
            
				IntPtr list = GLibUtil.CalComponentTextArrayToGSList (val);
				e_cal_component_set_comment_list (Handle, list);
				GLibUtil.FreeCalComponentTextGSList (list);
			}
		}

		[DllImport("ecal")]
			static extern void e_cal_component_get_categories_list (IntPtr raw, out IntPtr list);
		[DllImport("ecal")]
			static extern void e_cal_component_set_categories_list (IntPtr raw, IntPtr list);

		public string[] Categories {
			get {
				IntPtr raw_ret = IntPtr.Zero;
				e_cal_component_get_categories_list(Handle, out raw_ret);
				return CalUtil.GSListToStringArray (raw_ret, true);
			}
			set 	
			{
				IntPtr list = GLibUtil.StringArrayToGSList (value);
				e_cal_component_set_categories_list (Handle, list);
				GLibUtil.FreeGSList (list);
			}
		}
      
		[DllImport("ecal")]
			static extern string e_cal_component_get_as_string (IntPtr handle);
		public string GetAsString ()
		{
			string ical = e_cal_component_get_as_string (handle);

			return ical;
		}
		
#if USE_EDS_2_24
		[DllImport("ecal")]
			static extern int e_cal_component_get_sequence_as_int (IntPtr handle);
		[DllImport("ecal")]
			static extern void e_cal_component_set_sequence (IntPtr handle, ref int set);
		public int Sequence {
			get 
			{
				int ret;
				int raw = e_cal_component_get_sequence_as_int (Handle);
				ret = raw;

				return ret;
				
			}
			set
			{
				e_cal_component_set_sequence (Handle, ref value);
			}
		}
#endif
		
		[DllImport("ecal")]
			static extern void e_cal_component_commit_sequence (IntPtr handle);
		[DllImport("ecal")]
			static extern int e_cal_component_abort_sequence (IntPtr handle);
		[DllImport("ecal")]
			static extern IntPtr icalcomponent_new_from_string (string str);
		[DllImport("ecal")]
			static extern bool e_cal_modify_object (IntPtr ecal, IntPtr ical, CalObjModType mode, out IntPtr error);
		[DllImport("ecal")]
			static extern bool e_cal_create_object (IntPtr ecal, IntPtr ical, out string uid, out IntPtr error);
		[DllImport("ecal")]
			static extern void icalcomponent_free (IntPtr handle);
      
		public void Commit()
		{
			IntPtr ical = IntPtr.Zero;
			IntPtr error = IntPtr.Zero;
			string uid = "";
			bool commited = false;

			e_cal_component_commit_sequence (Handle);

			if (ecal == null)
				return;

			string str = e_cal_component_get_as_string (Handle);
         
			if (str != null)
				if (str.Length > 0)
					if ((ical = icalcomponent_new_from_string (str)) != IntPtr.Zero) {
                
						if (!new_component)
							commited = e_cal_modify_object (ECal.Handle, ical, CalObjModType.This, out error);
						else {
							commited = e_cal_create_object (ECal.Handle, ical, out uid, out error);
							new_component = ! commited;
						}
                
						icalcomponent_free (ical);
					}
         
			if (!commited)
				e_cal_component_abort_sequence (Handle);
         
			if (error != IntPtr.Zero)
				throw new GLib.GException( error );
		}
	    [DllImport("evolutionglue")]
		static extern void e_cal_glue_free_cal_component (IntPtr handle);
	    ~CalComponent()
	    {
		e_cal_glue_free_cal_component (handle);
	    }
		
#if USE_EDS_2_24
	    [DllImport("ecal")]
			static extern void e_cal_component_get_rrule_list (IntPtr handle, out IntPtr list);
	    [DllImport("ecal")]
		    static extern void e_cal_component_set_rrule_list (IntPtr handle, IntPtr list);
	
	    public CalRecurrence [] RRules
	    {
		    get 
		    {
			    IntPtr raw_ret = IntPtr.Zero;
			    IntPtr iter = IntPtr.Zero;
			    int i = 0;
			    int len = 0;
			    CalRecurrence[] ret = null;

			    e_cal_component_get_rrule_list (Handle, out raw_ret);

			    if (raw_ret != IntPtr.Zero) {
				    if ((len = GLibUtil.g_slist_length (raw_ret)) > 0 ) {
					    ret = new CalRecurrence [len];

					    for (i = 0, iter=raw_ret; iter != IntPtr.Zero; 
							    iter = GLibUtil.gtksharp_slist_get_next (iter), i++) {
						    IntPtr data = GLibUtil.gtksharp_slist_get_data (iter);
						    CalRecurrence r = (CalRecurrence) Marshal.PtrToStructure (data, typeof (CalRecurrence));

						    ret [i] = r;
					    }
				    }
				    GLibUtil.g_slist_free (raw_ret);
			    }
			    return ret == null ? new CalRecurrence [0] : ret;
		    }
		    set 
		    {
			    IntPtr ptr = IntPtr.Zero;
			    IntPtr raw = IntPtr.Zero;

			    foreach (CalRecurrence r in value) {
				    ptr = Marshal.AllocHGlobal (Marshal.SizeOf (r));
				    Marshal.StructureToPtr (r, ptr, false);
				    raw = GLibUtil.g_slist_append (raw, ptr);
			    }

			    e_cal_component_set_rrule_list (Handle, raw);
			    GLibUtil.g_slist_free (raw);
		    }
	    }

	    [DllImport("ecal")]
		    static extern bool e_cal_component_has_rrules (IntPtr handle);
	    [DllImport("ecal")]
		    static extern bool e_cal_component_has_exdates (IntPtr handle);
	    [DllImport("ecal")]
		    static extern bool e_cal_component_is_instance (IntPtr handle);

	    public bool HasRRules 
	    {
		get {
			bool ret = e_cal_component_has_rrules (Handle);
			return ret;
		}
	    } 

	    public bool HasExDates 
	    {
		get {
			bool ret = e_cal_component_has_exdates (Handle);
			return ret;
		}
	    }

	    public bool IsInstance
	    {
		get
		{
			bool ret = e_cal_component_is_instance (Handle);
			return ret;
		}
	    }
	    
	    [DllImport("ecal")]
		    static extern void e_cal_component_get_exdate_list (IntPtr handle, out IntPtr list);
	    [DllImport("ecal")]
		    static extern void e_cal_component_set_exdate_list (IntPtr handle, IntPtr list);

	    public CalComponentDateTime[] ExDates
	    {
		get
		{
			IntPtr raw_ret = IntPtr.Zero;
			IntPtr iter = IntPtr.Zero;
			int i = 0;
			int len = 0;
			CalComponentDateTime [] ret = null;
		
			e_cal_component_get_exdate_list (Handle, out raw_ret);
			if (raw_ret != IntPtr.Zero) {
				    if ((len = GLibUtil.g_slist_length (raw_ret)) > 0 ) {
					    ret = new CalComponentDateTime [len];

					    for (i = 0, iter=raw_ret; iter != IntPtr.Zero; 
							    iter = GLibUtil.gtksharp_slist_get_next (iter), i++) {
						    IntPtr data = GLibUtil.gtksharp_slist_get_data (iter);
						    CalComponentDateTime d = (CalComponentDateTime) Marshal.PtrToStructure (data, typeof (CalComponentDateTime));

						    ret [i] = d;
					    }
				    }
				    GLibUtil.g_slist_free (raw_ret);
			    }
			    return ret == null ? new CalComponentDateTime [0] : ret;

		}
		set
		{
			IntPtr ptr = IntPtr.Zero;
			IntPtr raw = IntPtr.Zero;

			foreach (CalComponentDateTime d in value) {
				ptr = Marshal.AllocHGlobal (Marshal.SizeOf (d));
				Marshal.StructureToPtr (d, ptr, false);
				raw = GLibUtil.g_slist_append (raw, ptr);
			}

			e_cal_component_set_exdate_list (Handle, raw);
			GLibUtil.g_slist_free (raw);
		}
	    }
	    
	    [DllImport("ecal")]
		    static extern void e_cal_component_set_x_prop (IntPtr handle, string name, string val);
	    [DllImport("ecal")]
		    static extern string e_cal_component_get_x_prop (IntPtr handle, string name);
	    [DllImport("ecal")]
		    static extern int e_cal_component_get_x_prop_as_int (IntPtr handle, string name);

	    public void SetXProp (string name, string val)
	    {
		e_cal_component_set_x_prop (Handle, name, val);
	    }

	    public string GetXProp (string name)
	    {
		string ret = e_cal_component_get_x_prop (Handle, name);

		return ret;
	    }

	    public int GetXPropAsInt (string name)
	    {
		int ret = e_cal_component_get_x_prop_as_int (Handle, name);

		return ret;
	    }
#endif
	}
}

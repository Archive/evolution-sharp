using System;
using System.Runtime.InteropServices;

namespace Evolution {
	public enum CalMode : int {
		Invalid = -1,
		Local = 1 << 0,
		Remote = 1 << 1,
		Any= 0x07
	}

	public enum BackendStatus {
		Success,
		RepositoryOffline,
		PermissionDenied,
		InvalidRange,
		ObjectNotFound,
		InvalidObject,
		ObjectIdAlreadyExists,
		AuthenticationFailed,
		AuthenticationRequired,
		UnsupportedField,
		UnsupportedMethod,
		UnsupportedAuthenticationMethod,
		TLSNotAvailable,
		NoSuchCal,
		UnknownUser,
		OfflineUnavailable,
		SearchSizeLimitExceeded,
		SearchTimeLimitExceeded,
		InvalidQuery,
		QueryRefused,
		CouldNotCancel,
		OtherError,
		InvalidServerVersion
	}


	public class CalUtil : GLibUtil {
/*
		public static CalRecurrence[] GLibSListToCalRecurrenceArray (IntPtr slist)
		{
			int i = 0;
			IntPtr iter = IntPtr.Zero;
			int len = 0;

			len = g_slist_length (slist);
			CalRecurrence[] rrules = new CalRecurrence [len];

			for (i = 0, iter=slist; iter != IntPtr.Zero; iter = gtksharp_slist_get_next (iter), i++) {
				IntPtr data = gtksharp_slist_get_data (iter);
				CalRecurrence recur = new CalRecurrence ();
				recur.Frequency = CalGlueRecurrenceUtil.GetFrequency (data);
				recur.Count  = CalGlueRecurrenceUtil.GetCount (data);
				recur.Enddate = CalGlueRecurrenceUtil.GetEnddate (data);
				recur.WeekStartDay = CalGlueRecurrenceUtil.GetWeekStartDay (data);
				recur.Interval = CalGlueRecurrenceUtil.GetInterval (data);
				recur.BySecond = CalGlueRecurrenceUtil.GetBySecond (data);
				recur.ByMinute = CalGlueRecurrenceUtil.GetByMinute (data);
				recur.ByHour = CalGlueRecurrenceUtil.GetByHour (data);
				recur.ByDay = CalGlueRecurrenceUtil.GetByDay (data);
				recur.ByMonthDay = CalGlueRecurrenceUtil.GetByMonthDay (data);
				recur.ByYearDay = CalGlueRecurrenceUtil.GetByYearDay (data);
				recur.ByWeekNumber = CalGlueRecurrenceUtil.GetByWeekNumber (data);
				recur.ByMonth = CalGlueRecurrenceUtil.GetByMonth (data);
				recur.BySetPos = CalGlueRecurrenceUtil.GetBySetPos (data);
				rrules [i] = recur;
			}
			return rrules;
		}
*/
		public static DateTime MinDate {
			get { return local_epoch; }
		}

		public static DateTime MaxDate {
			get { return local_epoch.AddSeconds (Int32.MaxValue); }
		}

		public static String GetQueryStringForTimeRange (DateTime start, DateTime end)
		{
			// Evo doesn't like dates before the epoch or after the max time, so
			// check for those and throw an exception.
			if (start < MinDate)
				throw new ArgumentOutOfRangeException ("start");

			if (end > MaxDate)
				throw new ArgumentOutOfRangeException ("end");

			string query;
			string strDtstart;
			string strDtend;
			strDtstart = String.Format ("{0:0000}{1:00}{2:00}T{3:00}{4:00}{5:00}Z",
							start.Year,
							start.Month,
							start.Day,
							start.Hour,
							start.Minute,
							start.Second);

			strDtend = String.Format ("{0:0000}{1:00}{2:00}T{3:00}{4:00}{5:00}Z",
							end.Year,
							end.Month,
							end.Day,
							end.Hour,
							end.Minute,
							end.Second);

			
			query = "(occur-in-time-range? (make-time \"" 
				+ strDtstart + "\")" 
				+ " (make-time \"" 
				+ strDtend + "\"))";
			
			return query;
		}

		[DllImport("ecal")]
		static extern IntPtr e_cal_component_clone (IntPtr raw);
		[DllImport("ecal")]
		static extern void e_cal_free_object_list (IntPtr raw);
      
		public static CalComponent[] ECalToCalComponentArray (IntPtr ecal_objects, Cal ecal)
		{
			int i = 0;
			IntPtr iter = IntPtr.Zero;

			int len = g_slist_length (ecal_objects);
			CalComponent[] compArray = new CalComponent [len];

			for (i = 0, iter=ecal_objects; iter != IntPtr.Zero; iter = gtksharp_slist_get_next (iter), i++)
				compArray[i] = new CalComponent (e_cal_component_clone (gtksharp_slist_get_data (iter)), ecal);

			return compArray;
		}

		[DllImport("ecal")]
			static extern int icaltime_days_in_month (int month, int year);

		public static int DaysInMonth (int month, int year)
		{
			return icaltime_days_in_month (month, year);
		}

		[DllImport("ecal")]
			static extern void icalcomponent_free (IntPtr handle);
		[DllImport("ecal")]
			static extern IntPtr e_cal_component_new ();
		[DllImport("ecal")]
			static extern IntPtr icalcomponent_new_clone (IntPtr component);
		[DllImport("ecal")]
			static extern bool e_cal_component_set_icalcomponent (IntPtr ecal, IntPtr ical);

		public static CalComponent[] ICalToCalComponentArray (IntPtr ecal_objects, Cal ecal)
		{
			int i = 0;
			IntPtr iter = IntPtr.Zero;

			int len = g_slist_length (ecal_objects);
			CalComponent[] compArray = new CalComponent [len];

			for (i = 0, iter=ecal_objects; iter != IntPtr.Zero; iter = gtksharp_slist_get_next (iter), i++) {
				IntPtr clone = icalcomponent_new_clone (gtksharp_slist_get_data (iter));
				IntPtr comp = e_cal_component_new ();
				e_cal_component_set_icalcomponent (comp, clone);
				compArray[i] = new CalComponent ( comp, ecal );
			}

			return compArray;
		}

		[DllImport("evolutionglue")]
		public static extern void e_cal_glue_free_icaltimetype (IntPtr d);
		[DllImport("evolutionglue")]
		public static extern void e_cal_glue_free_ecalcomponentdatetime (IntPtr d);

		[DllImport("evolutionglue")]
		static extern int e_cal_icaltimetype_to_timet (IntPtr icaltimetype, bool freemem);
		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_timet_to_icaltimetype (int timet);
		[DllImport("evolutionglue")]
		static extern int e_cal_ecalcomponentdatetime_to_timet (IntPtr ecaldatetime, bool freemem);
		[DllImport("evolutionglue")]
		static extern IntPtr e_cal_timet_to_ecalcomponentdatetime (int timet);

		[DllImport("evolutionglue")]
		public static extern IntPtr e_cal_alloc_ecalcomponentdatetime ();

		[DllImport("evolutionglue")]
		public static extern void e_cal_free_ecalcomponentdatetime (IntPtr dt);
      
		[DllImport("evolutionglue")]
		public static extern IntPtr e_cal_alloc_icaltimetype ();

		[DllImport("evolutionglue")]
		public static extern void e_cal_free_icaltimetype (IntPtr dt);
		
		public static DateTime icaltimetype_to_datetime (IntPtr ptr, bool free_mem)
		{
			int d = 0;
			DateTime ret = DateTime.MinValue;
         
			if (ptr != IntPtr.Zero)
				if ((d = e_cal_icaltimetype_to_timet (ptr, free_mem)) != 0)
					ret = GLibUtil.Unixtime_tToDateTime (d);
         
			return ret;
		}

		public static IntPtr datetime_to_icaltimetype (DateTime dt)
		{
			return e_cal_timet_to_icaltimetype (GLibUtil.DateTimeToUnixtime_t (dt));
		}

		public static DateTime ecalcomponentdatetime_to_datetime (IntPtr ptr)
		{
			int d = 0;
			DateTime ret = DateTime.MinValue;
         
			if (ptr != IntPtr.Zero)
				if ((d = e_cal_ecalcomponentdatetime_to_timet (ptr, true)) != 0)
					ret = GLibUtil.Unixtime_tToDateTime (d);
         
			return ret;
		}
      
		public static IntPtr datetime_to_ecalcomponentdatetime (DateTime dt)
		{
			return e_cal_timet_to_ecalcomponentdatetime (GLibUtil.DateTimeToUnixtime_t (dt));
		}
	}
}

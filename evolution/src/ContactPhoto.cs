/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Copyright (C) 2006 Novell, Inc.
 *
 * Author:  Veerapuram Varadhan  <vvaradhan@novell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

namespace Evolution {

	using System;
	using System.Collections;
	using System.Runtime.InteropServices;

	public class ContactPhoto {
		
		IntPtr Handle;

		[DllImport("evolutionglue")]
		static extern void e_contact_photo_glue_free (IntPtr raw);

		~ContactPhoto()
		{
			e_contact_photo_glue_free (Handle);
		}
		
		[DllImport("evolutionglue")]
		static extern void dump_e_contact_photo_glue (IntPtr raw);

		[DllImport("gobject-2.0")]
		static extern IntPtr g_boxed_copy (GLib.GType type, IntPtr raw);

		public ContactPhoto(IntPtr raw)
		{
			if (raw != IntPtr.Zero)
				Handle = g_boxed_copy (ContactPhoto.GType, raw);
			else
				Handle = raw;
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_contact_photo_glue_new(ContactPhotoType type);
		
		public ContactPhoto (ContactPhotoType type)
		{
			Handle = e_contact_photo_glue_new (type);
		}

		[DllImport("ebook")]
		static extern IntPtr e_contact_photo_get_type();

		public static GLib.GType GType { 
			get {
				IntPtr raw_ret = e_contact_photo_get_type();
				GLib.GType ret = new GLib.GType(raw_ret);
				return ret;
			}
		}

		[DllImport("glibsharpglue-2")]
		static extern IntPtr glibsharp_value_get_boxed (ref GLib.Value val);

		[DllImport("glibsharpglue-2")]
		static extern void glibsharp_value_set_boxed (ref GLib.Value val, ref Evolution.ContactPhoto boxed);

		public static explicit operator GLib.Value (Evolution.ContactPhoto boxed)
		{
			GLib.Value val = GLib.Value.Empty;
			val.Init (Evolution.ContactPhoto.GType);
			glibsharp_value_set_boxed (ref val, ref boxed);
			return val;
		}

		public static explicit operator Evolution.ContactPhoto (GLib.Value val)
		{
			IntPtr boxed_ptr = glibsharp_value_get_boxed (ref val);

			return new ContactPhoto (boxed_ptr);
		}

		[DllImport("evolutionglue")]
		static extern ContactPhotoType e_contact_photo_glue_get_photo_type(IntPtr raw);

		[DllImport("evolutionglue")]
		static extern void e_contact_photo_glue_set_photo_type(IntPtr raw, ContactPhotoType type);
		
		public ContactPhotoType PhotoType {
			get {
				return e_contact_photo_glue_get_photo_type (Handle);
			}
			set {
				e_contact_photo_glue_set_photo_type (Handle, value);
			}

		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_contact_photo_glue_get_mime_type(IntPtr raw);

		[DllImport("evolutionglue")]
		static extern void e_contact_photo_glue_set_mime_type(IntPtr raw, string value);

		public string MimeType {
			get {
				IntPtr raw_ret = e_contact_photo_glue_get_mime_type(Handle);
				if (raw_ret == IntPtr.Zero)
					return null;

				string ret = Marshal.PtrToStringAnsi (raw_ret);
				return ret;
			}
			set {
				e_contact_photo_glue_set_mime_type (Handle, value);
			}
		}

		[DllImport("evolutionglue")]
		static extern int e_contact_photo_glue_get_length(IntPtr raw);

		[DllImport("evolutionglue")]
		static extern void e_contact_photo_glue_set_length(IntPtr raw, int value);

		public int Length {
			get {
				return e_contact_photo_glue_get_length (Handle);
			}
			set {
				e_contact_photo_glue_set_length (Handle, value);
			}
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_contact_photo_glue_get_uri(IntPtr raw);

		[DllImport("evolutionglue")]
		static extern void e_contact_photo_glue_set_uri(IntPtr raw, string value);

		public string Uri {
			get {
				IntPtr raw_ret = e_contact_photo_glue_get_uri (Handle);
				if (raw_ret == IntPtr.Zero)
					return null;

				string ret = Marshal.PtrToStringAnsi (Handle);
				return ret;
			}
			set {
				e_contact_photo_glue_set_uri (Handle, value);
			}
		}

		[DllImport("evolutionglue")]
		static extern IntPtr e_contact_photo_glue_get_data(IntPtr raw);

		[DllImport("evolutionglue")]
		static extern void e_contact_photo_glue_set_data(IntPtr raw, byte[] data);

		// Returned data should be freed by the caller.
		public byte[] Data {
			get {
				IntPtr raw_ret = e_contact_photo_glue_get_data (Handle);
				if (raw_ret == IntPtr.Zero)
					return null;

				int len = Length;
				if (len == -1)
					return null;

				byte[] data = new byte [len];

				Marshal.Copy (raw_ret, data, 0, len);
				return data;
			}
			set {
				e_contact_photo_glue_set_data (Handle, value);
			}
		}
	}
}

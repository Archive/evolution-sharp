namespace Evolution {

	using System;

	public delegate void WritableStatusHandler(object o, WritableStatusArgs args);

	public class WritableStatusArgs : GLib.SignalArgs {
		public bool Writable{
			get {
				return (bool) Args[0];
			}
		}

	}

	public delegate void ViewProgressHandler(object o, ViewProgressArgs args);

	public class ViewProgressArgs : GLib.SignalArgs {
		public string Message{
			get {
				return (string) Args[0];
			}
		}

		public int Percent{
			get {
				return (int) Args[1];
			}
		}

	}

	public delegate void ViewDoneHandler(object o, ViewDoneArgs args);

	public class ViewDoneArgs : GLib.SignalArgs {
		public Evolution.CalendarStatus Status{
			get {
				return (Evolution.CalendarStatus) Args[0];
			}
		}

	}

	public delegate void StatusMessageHandler(object o, StatusMessageArgs args);

	public class StatusMessageArgs : GLib.SignalArgs {
		public string Message{
			get {
				return (string) Args[0];
			}
		}

	}

	public delegate void SourceAddedHandler(object o, SourceAddedArgs args);

	public class SourceAddedArgs : GLib.SignalArgs {
		public Evolution.Source Source{
			get {
				return (Evolution.Source) Args[0];
			}
		}

	}

	public delegate void SourceRemovedHandler(object o, SourceRemovedArgs args);

	public class SourceRemovedArgs : GLib.SignalArgs {
		public Evolution.Source Source{
			get {
				return (Evolution.Source) Args[0];
			}
		}

	}

	public delegate void SequenceCompleteHandler(object o, SequenceCompleteArgs args);

	public class SequenceCompleteArgs : GLib.SignalArgs {
		public Evolution.BookViewStatus Status{
			get {
				return (Evolution.BookViewStatus) Args[0];
			}
		}

	}

	public delegate void RemovedHandler(object o, RemovedArgs args);

	public class RemovedArgs : GLib.SignalArgs {
		public int Status{
			get {
				return (int) Args[0];
			}
		}

	}

	public delegate void OpenedHandler(object o, OpenedArgs args);

	public class OpenedArgs : GLib.SignalArgs {
		public int Status{
			get {
				return (int) Args[0];
			}
		}

	}

	public delegate void ObjectsAddedHandler(object o, ObjectsAddedArgs args);

	public class ObjectsAddedArgs : GLib.SignalArgs {
		public GLib.List Objects{
			get {
				return (GLib.List) Args[0];
			}
		}

	}

	public delegate void ObjectsRemovedHandler(object o, ObjectsRemovedArgs args);

	public class ObjectsRemovedArgs : GLib.SignalArgs {
		public GLib.List Uids{
			get {
				return (GLib.List) Args[0];
			}
		}

	}

	public delegate void ObjectsModifiedHandler(object o, ObjectsModifiedArgs args);

	public class ObjectsModifiedArgs : GLib.SignalArgs {
		public GLib.List Objects{
			get {
				return (GLib.List) Args[0];
			}
		}

	}

	public delegate void GroupRemovedHandler(object o, GroupRemovedArgs args);

	public class GroupRemovedArgs : GLib.SignalArgs {
		public Evolution.SourceGroup Group{
			get {
				return (Evolution.SourceGroup) Args[0];
			}
		}

	}

	public delegate void GroupAddedHandler(object o, GroupAddedArgs args);

	public class GroupAddedArgs : GLib.SignalArgs {
		public Evolution.SourceGroup Group{
			get {
				return (Evolution.SourceGroup) Args[0];
			}
		}

	}

	public delegate void ContactsAddedHandler(object o, ContactsAddedArgs args);

	public class ContactsAddedArgs : GLib.SignalArgs {
		public GLib.List Contacts{
			get {
				return (GLib.List) Args[0];
			}
		}

	}

	public delegate void ContactsRemovedHandler(object o, ContactsRemovedArgs args);

	public class ContactsRemovedArgs : GLib.SignalArgs {
		public GLib.List Ids{
			get {
				return (GLib.List) Args[0];
			}
		}

	}

	public delegate void ContactsChangedHandler(object o, ContactsChangedArgs args);

	public class ContactsChangedArgs : GLib.SignalArgs {
		public GLib.List Contacts{
			get {
				return (GLib.List) Args[0];
			}
		}

	}

	public delegate void ConnectionStatusHandler(object o, ConnectionStatusArgs args);

	public class ConnectionStatusArgs : GLib.SignalArgs {
		public bool Connected{
			get {
				return (bool) Args[0];
			}
		}

	}

	public delegate void CalOpenedHandler(object o, CalOpenedArgs args);

	public class CalOpenedArgs : GLib.SignalArgs {
		public Evolution.CalendarStatus Status{
			get {
				return (Evolution.CalendarStatus) Args[0];
			}
		}

	}

	public delegate void BackendErrorHandler(object o, BackendErrorArgs args);

	public class BackendErrorArgs : GLib.SignalArgs {
		public string Message{
			get {
				return (string) Args[0];
			}
		}

	}

}

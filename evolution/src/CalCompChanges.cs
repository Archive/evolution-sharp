using System;
using Evolution;
//using System.Collections.Generic;
using System.Collections;


namespace Evolution 
{
	public class CalCompChanges 
	{
		private CalComponent oComp, nComp;
		
		private bool CheckStringChanged (string oVal, string nVal)
		{
			if (oVal == nVal)
				return false;

			return true;
		}
		
		private bool CheckDateTimeChanged (CalComponentDateTime oDt, CalComponentDateTime nDt)
		{
			DateTime oldTime = oDt.IcalTime.AsDateTimeUtc;
			DateTime newTime = nDt.IcalTime.AsDateTimeUtc;
				
			if (CheckStringChanged (oDt.Tzid, nDt.Tzid))
				return true;
			else if (DateTime.Compare (oldTime, newTime) != 0)
				return true;
			else
				return false;
		}

		
		public CalCompChanges (CalComponent oldComp, CalComponent newComp)
		{
			oComp = oldComp;
			nComp = newComp;	
		}
		
		public bool Description 
		{
			get 
			{
				string [] oDescriptions = oComp.Descriptions;
				string [] nDescriptions = nComp.Descriptions;
				
				if (oDescriptions.Length != nDescriptions.Length)
					return true;
				else if (oDescriptions.Length == 0)
					return false;

				return CheckStringChanged (oDescriptions [0], nDescriptions  [0]);
			}
		}

		public bool Summary
		{
			get
			{
				return CheckStringChanged (oComp.Summary, nComp.Summary);
			}
		}
		
		public bool Location
		{
			get 
			{
				return CheckStringChanged (oComp.Location, nComp.Location);
			}
		}

		public bool StartDate
		{
			get
			{
				return CheckDateTimeChanged (oComp.DtStart, nComp.DtStart);
			}
		}

		public bool EndDate
		{
			get
			{
				return CheckDateTimeChanged (oComp.DtEnd, nComp.DtEnd);
			}
		}

		public bool RRules
		{
			get
			{
				if (oComp.HasRRules != nComp.HasRRules)
					return true;
				else if (oComp.HasRRules == false)
					return false;
				
				CalRecurrence [] oRecur = oComp.RRules;
				CalRecurrence [] nRecur = nComp.RRules;

				string oRrule = oRecur [0].ToString ();
				string nRrule = nRecur [0].ToString ();

				return CheckStringChanged (oRrule, nRrule);
			}
		}

		public bool AllDay
		{
			get
			{
				if (oComp.IsAllDay != nComp.IsAllDay)
					return true;
				else 
					return false;
			}
		}

	}
}

using System;
using System.Collections;
using System.Runtime.InteropServices;      

// TODO: (Also, we need exception-rules support.
// As of now, this provides a very basic support, (ie),
// represent whatever is there in the underlying e-d-s, AS IS.
// More "resonable" interpretation is yet to be done.
	
namespace Evolution {

	//
	// Recurrence enumerations
	//

	public enum FrequencyType {
		//
		// Please do not change the order/values of the items.
		//

		SECONDLY=0,
		MINUTELY=1,
		HOURLY=2,
		DAILY=3,
		WEEKLY=4,
		MONTHLY=5,
		YEARLY=6,
		FREQUENCY_NO=7
	};

	public enum WeekDayType {
		NO_WEEKDAY,
		SUNDAY,
		MONDAY,
		TUESDAY,
		WEDNESDAY,
		THURSDAY,
		FRIDAY,
		SATURDAY
	};

	public enum ByValuesType {
		SECOND,
		MINUTE,
		HOUR,
		DAY,
		MONTH_DAY,
		YEAR_DAY,
		WEEK_NUMBER,
		MONTH,
		SET_POS
	};

	[StructLayout(LayoutKind.Sequential)]
	public class CalRecurrence {
		/* dummy variable to not break ABI */
		public IntPtr Handle;

		public FrequencyType Frequency;
		
		Icaltimetype until;
		public int Count;
		
		public short Interval;
		
		public WeekDayType WeekStartDay;
		
		[MarshalAs (UnmanagedType.ByValArray, SizeConst=61)]
		public short[] BySecond;
		
		[MarshalAs (UnmanagedType.ByValArray, SizeConst=61)]
		public short[] ByMinute;

		[MarshalAs (UnmanagedType.ByValArray, SizeConst=25)]
		public short[] ByHour;

		[MarshalAs (UnmanagedType.ByValArray, SizeConst=364)]
		public short[] ByDay;

		[MarshalAs (UnmanagedType.ByValArray, SizeConst=32)]
		public short[] ByMonthDay;

		[MarshalAs (UnmanagedType.ByValArray, SizeConst=367)]
		public short[] ByYearDay;

		[MarshalAs (UnmanagedType.ByValArray, SizeConst=54)]
		public short[] ByWeekNumber;
		
		[MarshalAs (UnmanagedType.ByValArray, SizeConst=13)]
		public short[] ByMonth;

		[MarshalAs (UnmanagedType.ByValArray, SizeConst=367)]
		public short[] BySetPos;

		private short[] GetInitializedArray (int len)
		{
			short [] ret = new short [len];

			for (int i = 0; i < len; i++)
			{
				ret [i]	= 0x7f;
			}
			ret [0] = 0x7f7f;
			return ret;
		}

		public CalRecurrence ()
		{
			Frequency = FrequencyType.FREQUENCY_NO;
			Count = 0;
			//week_start_day = -1;
			Interval = 1;
			WeekStartDay = WeekDayType.NO_WEEKDAY;
			until = new Icaltimetype ();
			BySecond = GetInitializedArray (61);
			ByMinute = GetInitializedArray (61);
			ByHour = GetInitializedArray (25);
			ByDay = GetInitializedArray (364);
			ByMonthDay = GetInitializedArray (32);
			ByYearDay = GetInitializedArray (367);
			ByWeekNumber = GetInitializedArray (54);
			ByMonth = GetInitializedArray (13);
			BySetPos = GetInitializedArray (367);
		}

		public DateTime Until 
		{
			get
			{
				return until.AsDateTimeUtc;
			}
			set
			{
				until.SetDateTime = value;
			}
		}
		
		public static CalRecurrence Zero = new CalRecurrence ();

		[DllImport("ecal")]
			static extern IntPtr icalrecurrencetype_from_string (string str);

		public static CalRecurrence New(string recur)
		{
			IntPtr raw = icalrecurrencetype_from_string (recur);
			
			return (CalRecurrence) Marshal.PtrToStructure (raw, typeof (CalRecurrence));
		}


		~CalRecurrence ()
		{
		}

		[DllImport("ecal")]
			static extern string icalrecurrencetype_as_string(IntPtr recur);


		public string ToString ()
		{
			IntPtr native_dt = GLib.Marshaller.StructureToPtrAlloc (this);
			string ret  = icalrecurrencetype_as_string (native_dt);
			Marshal.FreeHGlobal (native_dt);

			return ret;
		}

		private static GLib.GType GType {
			get { return GLib.GType.Pointer; }
		}
	}
}

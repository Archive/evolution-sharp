using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace Evolution {


	[StructLayout(LayoutKind.Sequential)]
	public class CalComponentDateTime {

		private IntPtr _value;

		public Icaltimetype IcalTime {
			get 
			{ 
				return Icaltimetype.New (_value); 
			}
			set 
			{
				_value = GLib.Marshaller.StructureToPtrAlloc (value);
			}
		}
		public string Tzid;

		public static CalComponentDateTime Zero = new CalComponentDateTime ();

		public CalComponentDateTime () 
		{
		}

		public static CalComponentDateTime New(IntPtr raw) {
			if (raw == IntPtr.Zero) 
				return CalComponentDateTime.Zero;
			
			return (CalComponentDateTime) Marshal.PtrToStructure (raw, typeof (CalComponentDateTime));
		}

		private static GLib.GType GType {
			get { return GLib.GType.Pointer; }
		}
	}
}

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Copyright (C) 2006 Novell, Inc.
 *
 * Author:  Alois Belaska <lloyd@centrum.cz>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

using System;
using System.Collections;
using Gtk;
using GLib;
using Evolution;

namespace CalBindingsTest {
	public class CalendarBindingsTest {
		public static void Main (string[] args) {
			Application.Init ();

			SourceList slist = new SourceList ("/apps/evolution/tasks/sources");
			if (slist == null) {
				Console.WriteLine ("SourceList is null, quitting..");
				return;
			}
         
			SList group_list = slist.Groups;
			Console.WriteLine ("Group count: {0}", group_list.Count);
			foreach (SourceGroup group in group_list) {
				Console.WriteLine ("\nGroup UID:{0}, Name:{1}", group.Uid, group.Name);
				SList src_list = group.Sources;
				foreach (Evolution.Source src in src_list) {
					Cal cal = new Cal (src, CalSourceType.Todo);
					if (!cal.Open (true)) {
						Console.WriteLine ("Open failed");
						continue;
					}

					CalView cquery = cal.GetCalView ("#t");
					if (cquery == null) {
						Console.WriteLine ("Query object creation failed");
						continue;
					} else {
						cquery.Start ();
					}

					CalComponent[] event_list = cquery.Client.GetItems ("#t");  
               
					Console.WriteLine( "\n=================================" );
					Console.WriteLine ("Source Local:{0} UID:{1}, Name:{2}", src.IsLocal (), src.Uid, src.Name);
					Console.WriteLine( "=================================" );
               
					if (event_list == null) {
						Console.WriteLine ("No event strings found");
						continue;
					}
               
					/*CalComponent c = new CalComponent (cal);
					  c.Location = "lokace";
					  c.Summary = "summary";
					  c.Status = CalStatus.CAL_STATUS_INPROCESS;
					  c.Priority = CalPriority.CAL_PRIORITY_HIGH;
					  c.Classification = CalClassification.CAL_CLASS_PUBLIC;
					  c.Dtstart = DateTime.Now;
					  c.Dtend = DateTime.MinValue;
					  c.Dtstamp = DateTime.MinValue;
					  c.Due = new DateTime (2012,12,12,0,0,0);
					  c.Created = DateTime.Now;
					  c.Completed = DateTime.MinValue;
					  c.Commit();*/
					
					Console.WriteLine ("Find items {0}", event_list.Length);

					string[] sk = new string [2];
					sk [0] = "skupina100";
					sk [1] = "skupina111";
               
					string[] de = new string [2];
					de [0] = "description100";
					de [1] = "description111";
               
					string[] co = new string [2];
					co [0] = "comment100";
					co [1] = "comment111";

					int i = 0;
					foreach (CalComponent comp in event_list) {
						Console.WriteLine ("Saving {0}", ++i);
						comp.Location = "lokace 3";
						comp.Summary = "summary 3";
						comp.Status = CalStatus.InProcess;
						comp.Priority = CalPriority.High;
						comp.Classification = CalClassification.Public;
						comp.Dtstart = new DateTime (1990,9,9,0,0,0);
						comp.Dtend = new DateTime (2028,8,8,0,0,0);
						comp.Dtstamp = new DateTime (1999,10,10,0,0,0);
						comp.Due = new DateTime (2012,12,12,0,0,0);
						comp.Created = new DateTime (2012,12,12,0,0,0);
						comp.Completed = new DateTime (2012,12,12,0,0,0);
						comp.Categories = sk;
						comp.Descriptions = de;
						comp.Comments = co;
						comp.Commit();
					}
               
					i = 0;
					foreach (CalComponent comp in event_list) {
						Console.WriteLine ("\n------------------------");
						Console.WriteLine ("item {0}", ++i);
						Console.WriteLine ("UId: {0}", comp.Uid);
						Console.WriteLine ("RecurId: {0}", comp.RecurId);
						Console.WriteLine ("Summary: \"{0}\"", comp.Summary);
						Console.WriteLine ("Status: {0}", comp.Status);
						Console.WriteLine ("Priority: {0}", comp.Priority);
						Console.WriteLine ("Classification: {0}", comp.Classification);
						Console.WriteLine ("Location: \"{0}\"", comp.Location);
						Console.WriteLine ("Dtstart: {0}", comp.Dtstart );
						Console.WriteLine ("Dtend: {0}", comp.Dtend);
						Console.WriteLine ("Dtstamp: {0}", comp.Dtstamp);
						Console.WriteLine ("Due: {0}", comp.Due);
						Console.WriteLine ("LastModified: {0}", comp.LastModified);
						Console.WriteLine ("Created: {0}", comp.Created);
						Console.WriteLine ("Completed: {0}", comp.Completed);

						Console.Write( "Attendee:" );
						foreach (CalComponentAttendee att in comp.Attendees)
							Console.Write (" \"{0}\"", att.value);
						Console.WriteLine();
                  
						Console.Write( "Descriptions:" );
						foreach (string str in comp.Descriptions)
							Console.Write (" \"{0}\"", str);
						Console.WriteLine();

						Console.Write( "Categories:" );
						foreach (string str in comp.Categories)
							Console.Write (" \"{0}\"", str);
						Console.WriteLine();
                  
						Console.Write( "Comments:" );
						foreach (string str in comp.Comments)
							Console.Write (" \"{0}\"", str);
						Console.WriteLine( "End" );
					}
				}
			}
		}
	}
}

using System;
using System.Collections;
using Gtk;
using GLib;
using Evolution;

namespace BookBindingsTest {
	public class BookBindingsTest {


				/// <summary>
				/// Tests the Creation, Modification, and Deleting of a Contact in EDS
				/// </summary>
				public static void TestContact()
				{
					Console.WriteLine("TestContact: Opening System Addressbook");				
					Book systemBook = Book.NewSystemAddressbook();
					systemBook.Open(true);
			
					// ***********************************************
					// Book.AddContact Test
					// ***********************************************
					Console.WriteLine("TestContact: Creating contact EDS Test Dude");
					Contact contact = new Contact();
					contact.FileAs = "EDS Test Dude";
					
					string[] jabberId = new string[1];
					jabberId[0] = "edstestdude@jabber.org";
					contact.ImJabber = jabberId;
					contact.Email1 = "edstestdude@jabber.org";
					Console.WriteLine("TestContact: contact.Id before Add: {0}", contact.Id);
					systemBook.AddContact(contact);
					Console.WriteLine("TestContact: contact.Id after Add: {0}", contact.Id);
					
					// ***********************************************
					// Book.CommitContact Test
					// ***********************************************
					Console.WriteLine("TestContact: Modifying contact EDS Test Dude");					
					contact.BusinessPhone = "800.555.1212";
					contact.Nickname = "Mr. EDSContact Test Dude";
					
					systemBook.CommitContact(contact);
					
					
					// ***********************************************
					// Book.RemoveContact Test
					// ***********************************************
					Console.WriteLine("TestContact: Removing contact EDS Test Dude");
					systemBook.RemoveContact(contact.Id);
				}


				/// <summary>
				/// Tests the Creation, Modification, and Deleting of a ContactList in EDS
				/// </summary>
				public static void TestContactList()
				{
					Console.WriteLine("TestContactList: Opening System Addressbook");				
					Book systemBook = Book.NewSystemAddressbook();
					systemBook.Open(true);
			
					// ***********************************************
					// Create a contact to add to a ContactList
					// ***********************************************
					
					Console.WriteLine("TestContactList: Creating contact EDS Test Dude");
					Contact contact = new Contact();
					contact.FileAs = "EDS Test Dude";
					contact.Email1 = "edstestdude@jabber.org";

					Console.WriteLine("TestContactList: contact.Id before Add: {0}", contact.Id);
					systemBook.AddContact(contact);
					Console.WriteLine("TestContactList: contact.Id after Add: {0}", contact.Id);
					
					
					// ***********************************************
					// Create a ContactList
					// ***********************************************
					Console.WriteLine("TestContactList: Creating contactlist EDS Test List");
					Contact contactList = new Contact();
					contactList.List = true;					
					contactList.FileAs = "EDS Test List";

					Console.WriteLine("TestContactList: contactList.Id before Add: {0}", contactList.Id);
					systemBook.AddContact(contactList);
					Console.WriteLine("TestContactList: contactList.Id after Add: {0}", contactList.Id);


					// ***********************************************
					// Add the Contact to the ContactList
					// ***********************************************
					VCardAttribute attr = new VCardAttribute("", "EMAIL");
					VCardAttributeParam param = new VCardAttributeParam("X-EVOLUTION-DEST-CONTACT-UID");

					param.AddValue(contact.Id);
					attr.AddParam(param);
					param = new VCardAttributeParam("X-EVOLUTION-DEST-NAME");
					param.AddValue(contact.FileAs);
					attr.AddParam(param);
					param = new VCardAttributeParam("X-EVOLUTION-DEST-EMAIL");
					param.AddValue(contact.Email1);
					attr.AddParam(param);
					param = new VCardAttributeParam("X-EVOLUTION-DEST-HTML-MAIL");
					param.AddValue("FALSE");
					attr.AddParam(param);
					
					attr.AddValue(contact.FileAs + " <" + contact.Email1 + ">");
					contactList.AddAttribute(attr);
					
					systemBook.CommitContact(contactList);
					
					// ***********************************************
					// Clean everything up
					// ***********************************************
					Console.WriteLine("TestContactList: Cleaning up objects");
					systemBook.RemoveContact(contact.Id);
					systemBook.RemoveContact(contactList.Id);
				}




                private void OnContactsAdded (object o,
                                              Evolution.ContactsAddedArgs args)
                {
			Console.WriteLine ("Contacts added:");
			foreach (Contact contact in args.Contacts) {
				Console.WriteLine ("\nId: {0}", contact.Id);
				Console.WriteLine ("Fullname: {0}", contact.FullName);
				Console.WriteLine ("Photo: Mime-type: {0}", contact.Photo.MimeType);
				Console.WriteLine ("Photo: Uri: {0}", contact.Photo.Uri);
			}
                }

                private void OnContactsChanged (object o,
                                                Evolution.ContactsChangedArgs args)
                {

                }

                private void OnContactsRemoved (object o,
                                                Evolution.ContactsRemovedArgs args)
                {
			
		}

		public static void Main (string[] args)
		{
			Application.Init ();
	
			Console.WriteLine("Calling TestContact to test contact methods");
			BookBindingsTest.TestContact();

			Console.WriteLine("Calling TestContactList to test Contact List methods");			
			BookBindingsTest.TestContactList();

			SourceList slist = new SourceList ("/apps/evolution/addressbook/sources");
			if (slist != null) {
				SList group_list = slist.Groups;
				Console.WriteLine ("Group count: {0}", group_list.Count);
				foreach (SourceGroup group in group_list) {
					Console.WriteLine ("UID: {0}, Name: {1}", group.Uid, group.Name);
					SList src_list = group.Sources;
					foreach (Evolution.Source src in src_list) {
						Book bk = Book.NewSystemAddressbook ();
						//Book bk = new Book (src);
					       	bk.Open (true);

						Console.WriteLine ("UID: {0}, Name: {1}", src.Uid, src.Name);
						Evolution.BookQuery q = Evolution.BookQuery.AnyFieldContains ("");
						
						ArrayList dummy = new ArrayList ();
						BookView bk_view = bk.GetBookView (q, dummy, -1);

						BookBindingsTest t = new BookBindingsTest ();
						bk_view.ContactsChanged += t.OnContactsChanged;
						bk_view.ContactsAdded += t.OnContactsAdded;
						bk_view.ContactsRemoved += t.OnContactsRemoved;
							
						bk_view.Start ();
						
						Contact[] contactlist = bk.GetContacts (q);
						Console.WriteLine ("Contact count (range) : {0}", contactlist.Length);
												 
						
						if (contactlist != null) {
							foreach (Contact comp in contactlist) {
								Console.WriteLine ("Id: {0}", comp.Id);
								Console.WriteLine ("Fullname: {0}", comp.FullName);
								Console.WriteLine ("Photo: Mime-type: {0}", comp.Photo.MimeType);
								Console.WriteLine ("Photo: Uri: {0}", comp.Photo.Uri);
							}
						} else 
							Console.WriteLine ("No event strings found");

						Contact[] newcontacts;
						Contact[] updated;
						string[] removed;

						bk.GetChanges ("TestBook-testing", 
							       out newcontacts, 
							       out updated, 
							       out removed);
						if (newcontacts != null)
							Console.WriteLine ("Newly added: {0}", newcontacts.Length);

						if (updated != null)
							Console.WriteLine ("Updated : {0}", updated.Length);

						if (removed != null)
							Console.WriteLine ("removed added: {0}", removed.Length);
					}
					
				}
				
			}
			else
				Console.WriteLine ("slist is NULL");

			Application.Run ();
		}
	}
}

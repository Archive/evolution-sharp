using System;
using Gtk;
using GLib;
using Evolution;

namespace CalCacheBindingTest
{
	class CalCacheTest
	{
		public static void Main(string[] args)
		{
			Application.Init ();
			string uri = "chen", uid;
			CalBackendCache cache = new CalBackendCache (uri, CalSourceType.Event);
			CalComponent comp = new CalComponent (CalComponentVType.Event);

			/* Creating an Event */
			comp.Summary = "1st event";
		
			Icaltimezone zon = Icaltimezone.GetBuiltinTimezone ("America/New_York");
			CalComponentDateTime dt = new CalComponentDateTime ();
			Icaltimetype icalt= new Icaltimetype ();
			DateTime n = DateTime.Now.ToUniversalTime ();
			icalt.SetDateTime = n;
			icalt.IsUtc = 1;
			Console.WriteLine ("Before " + icalt.Hour + icalt.Minute);
			Icaltimezone.ConvertTime (icalt, Icaltimezone.UtcTimezone, zon);
			Console.WriteLine ("After " + icalt.Hour + icalt.Minute);
			dt.IcalTime = icalt;
			dt.Tzid = zon.Tzid;
			
			comp.DtStart = dt;	
			comp.Commit ();

			cache.PutComponent (comp);
			uid = comp.Uid;

			/* Inserting a default zone */
			cache.PutDefaultTimezone (zon);	

			/* Printing the default zone */
			Icaltimezone dzone = cache.DefaultTimezone;
			string dStr = "Default zone is cache is  " + dzone.Location + "\n";
			Console.WriteLine (dStr);

			Console.WriteLine ("Get all components ");
			CalComponent[] comps = cache.Components;
			Console.WriteLine (comps.Length);
			foreach (CalComponent c in comps)
			{
				Console.WriteLine (c.GetAsString ());
			}

			/* Printing the component */
			CalComponent cacheComp = cache.GetComponent (uid, null);
			dt = cacheComp.DtStart;
			Console.WriteLine("The value of DateTime from Cache \n");
			string dts = "\n" + dt.IcalTime.Year + dt.IcalTime.Month + dt.IcalTime.Day + dt.IcalTime.Hour + " " + dt.Tzid;
			Console.WriteLine(dts);

			Console.WriteLine("Removing the cache!");
			cache.Remove ();
		}
	}
}

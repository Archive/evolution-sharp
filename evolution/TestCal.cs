using System;
using System.Collections;
using Gtk;
using GLib;
using Evolution;

namespace CalBindingsTest {
	public class CalendarBindingsTest {
		
		public void OnItemsAdded (object o,
					  Evolution.ObjectsAddedArgs args)
		{
			CalComponent[] addedComp = CalUtil.ICalToCalComponentArray (args.Objects.Handle, ((CalView) o).Client);

			Console.WriteLine ("# of items Added: {0}", addedComp.Length);
			
			foreach (CalComponent comp in addedComp) {
				Console.WriteLine ("\nSource UId: {0}", comp.Source.Uid);
				Console.WriteLine (comp.Uid);
				foreach (string str in comp.Categories)
					Console.WriteLine (str);
				foreach (string str in comp.Descriptions)
					Console.WriteLine (str);
				foreach (string str in comp.Comments)
					Console.WriteLine (str);
				Console.WriteLine (comp.Dtstart);
				Console.WriteLine (comp.Dtend);
				
				// Use Dtstamp or Created or LastModified to
				// decide on the items to drop from reindexing.
				//
				// (ie), if LastModified is not a date after our
				// previous indexing, that calendar item shouldn't 
				// be re-indexed.  If LastModified is System.DateTime.MinValue,
				// Dtstamp should be checked.  If Dtstamp is System.DateTime.MinValue,
				// which is very unlikely, Created should be checked.
				Console.WriteLine ("LastModified: {0}", comp.LastModified);
				Console.WriteLine ("DTSTAMP: {0}", comp.Dtstamp);
				Console.WriteLine ("Created: {0}", comp.Created);
				Console.WriteLine ("Completed: {0}", comp.Completed);
				Console.WriteLine ("Due: {0}", comp.Due);
				Console.WriteLine ("Classification: {0}", comp.Classification);
				Console.WriteLine ("Priority: {0}", comp.Priority);
				Console.WriteLine ("Status: {0}", comp.Status);
				Console.WriteLine ("Location: {0}", comp.Location);
				Console.WriteLine ("summary: {0}", comp.Summary);
            
				Console.WriteLine( "Attendee:" );
				foreach (CalComponentAttendee att in comp.Attendees) {
					Console.WriteLine ("value: \"{0}\"", att.value);
					Console.WriteLine ("member: \"{0}\"", att.member);
					Console.WriteLine ("cutype: \"{0}\"", att.cutype);
					Console.WriteLine ("role: \"{0}\"", att.role);
					Console.WriteLine ("status: \"{0}\"", att.status);
					Console.WriteLine ("rsvp: \"{0}\"", att.rsvp);
					Console.WriteLine ("delto: \"{0}\"", att.delto);
					Console.WriteLine ("delfrom: \"{0}\"", att.delfrom);
					Console.WriteLine ("sentby: \"{0}\"", att.sentby);
					Console.WriteLine ("cn: \"{0}\"", att.cn);
					Console.WriteLine ("language: \"{0}\"", att.language);
					Console.WriteLine ("-----------");
				}
				if (comp.RRules == null) {
					Console.WriteLine ("No recurrence data found");
					return;
				}
				foreach (CalRecurrence recur in comp.RRules) {
					Console.WriteLine ("--- Recurrence data starts ---");

					Console.WriteLine ("Frequeny:- {0}", recur.Frequency);
					Console.WriteLine ("Count:- {0}", recur.Count);
					Console.WriteLine ("Interval:- {0}", recur.Interval);
//					Console.WriteLine ("Enddate:- {0}", recur.Until.DateTime);
					Console.WriteLine ("BySecond:- {0}", recur.BySecond);
					Console.WriteLine ("ByMinute:- {0}", recur.ByMinute);
					Console.WriteLine ("ByHour:- {0}", recur.ByHour);
					Console.WriteLine ("ByDay:- {0}", recur.ByDay);
					Console.WriteLine ("ByMonthDay:- {0}", recur.ByMonthDay);
					Console.WriteLine ("ByYearDay:- {0}", recur.ByYearDay);
					Console.WriteLine ("ByWeekNumber:- {0}", recur.ByWeekNumber);
					Console.WriteLine ("ByMonth:- {0}", recur.ByMonth);
					Console.WriteLine ("BySetPos:- {0}", recur.BySetPos);

					Console.WriteLine ("--- Recurrence Data ends ---");
					Console.WriteLine ("Recur string: \"{0}\"", recur.ToString());
				}

		}

		
			
			Console.WriteLine ("Returning from addedComp");
		}

		public void OnItemsModified (object o,
					     Evolution.ObjectsModifiedArgs args)
		{
			//AddItems (args.Objects);
			Console.WriteLine ("An item is modified");
			CalComponent[] addedComp = CalUtil.ICalToCalComponentArray (args.Objects.Handle, ((CalView) o).Client);
			Console.WriteLine ("# of items modified: {0}", addedComp.Length);
			foreach (CalComponent comp in addedComp) {
				foreach (string str in comp.Categories)
					Console.WriteLine (str);
				foreach (string str in comp.Descriptions)
					Console.WriteLine (str);
				foreach (string str in comp.Comments)
					Console.WriteLine (str);
				Console.WriteLine (comp.Dtstart);
				Console.WriteLine (comp.Dtend);
				Console.WriteLine ("LastModified: {0}", comp.LastModified);
				Console.WriteLine ("DTSTAMP: {0}", comp.Dtstamp);
				Console.WriteLine ("Created: {0}", comp.Created);
				Console.WriteLine ("Completed: {0}", comp.Completed);
				Console.WriteLine ("Due: {0}", comp.Due);
				Console.WriteLine ("Classification: {0}", comp.Classification);
				Console.WriteLine ("Priority: {0}", comp.Priority);
				Console.WriteLine ("Status: {0}", comp.Status);
				Console.WriteLine ("Location: {0}", comp.Location);
				Console.WriteLine ("summary: {0}", comp.Summary);

            Console.WriteLine( "Attendee:" );
            foreach (CalComponentAttendee att in comp.Attendees) {
               Console.WriteLine ("value: \"{0}\"", att.value);
               Console.WriteLine ("member: \"{0}\"", att.member);
               Console.WriteLine ("cutype: \"{0}\"", att.cutype);
               Console.WriteLine ("role: \"{0}\"", att.role);
               Console.WriteLine ("status: \"{0}\"", att.status);
               Console.WriteLine ("rsvp: \"{0}\"", att.rsvp);
               Console.WriteLine ("delto: \"{0}\"", att.delto);
               Console.WriteLine ("delfrom: \"{0}\"", att.delfrom);
               Console.WriteLine ("sentby: \"{0}\"", att.sentby);
               Console.WriteLine ("cn: \"{0}\"", att.cn);
               Console.WriteLine ("language: \"{0}\"", att.language);
               Console.WriteLine ("-----------");
            }
			}
		}

		public void OnItemsRemoved (object o,
					     Evolution.ObjectsRemovedArgs args)
		{
			//RemoveItems (args.Objects);
			Console.WriteLine ("An item is Removed");
		}

		public void OnViewProgress (object o,
					    Evolution.ViewProgressArgs args)
		{
			Console.WriteLine ("Progress: {0} {1}", args.Percent, args.Message);
		}

		public void OnViewDone (object o,
					Evolution.ViewDoneArgs args)
		{
			Console.WriteLine ("Done! Status: {0}", args.Status);
		}
					  

		public static void Main (string[] args)
		{
			Application.Init ();

			SourceList slist = new SourceList ("/apps/evolution/calendar/sources");
			if (slist == null) {
				Console.WriteLine ("SourceList is null, quitting..");
				return;
			}
			SList group_list = slist.Groups;
			Console.WriteLine ("Group count: {0}", group_list.Count);
			foreach (SourceGroup group in group_list) {
				Console.WriteLine ("UID: {0}, Name: {1}", group.Uid, group.Name);
				SList src_list = group.Sources;
				foreach (Evolution.Source src in src_list) {
					Console.WriteLine ("src Handle is {0}", src.Handle);

#if true
					Console.WriteLine ("ecal UID: {0}, Name: {1}", src.Uid, src.Name);

					Cal cal = new Cal (src, CalSourceType.Event);
					Console.WriteLine ("Cal Handle is {0}", cal.Handle);
					
					if (src.IsLocal ())
						Console.WriteLine ("Source {0} is a local source", src.Uid);
#else 
					Evolution.SourceGroup grp = new SourceGroup ("",group.BaseUri);
					Evolution.Source src1 = new Evolution.Source ("", src.RelativeUri);
					src1.Group = grp;
					Cal cal = new Cal (src1, CalSourceType.Event);
#endif
					cal.Open (true);

#if true
					CalendarBindingsTest t = new CalendarBindingsTest ();
					CalView cquery = null;
					cquery = cal.GetCalView ("#t");
					if (cquery == null)
						Console.WriteLine ("Query object creation failed");
					else {
						cquery.ObjectsModified += t.OnItemsModified;
						cquery.ObjectsAdded += t.OnItemsAdded;
						cquery.ObjectsRemoved += t.OnItemsRemoved;
						cquery.ViewProgress += t.OnViewProgress;
						cquery.ViewDone += t.OnViewDone;
							
						cquery.Start ();
					}
#else
					CalComponent[] event_list; /* = cal.GetItems (null);*/
					//Console.WriteLine ("Event count : {0}", event_list.Length);
					DateTime start = new DateTime (2005, 8, 1);
					DateTime end = new DateTime (2005, 8, 30);
					event_list = cal.GetItemsOccurInRange (start, end);
					Console.WriteLine ("Event count (range) : {0}", event_list.Length);
												 
						
					if (event_list == null) {
						Console.WriteLine ("No event strings found, returning...");
						return;
					}
					//string str;
					//IEnumerator iter = event_list.GetEnumerator ();
					//while (iter.MoveNext ()) {
					//str = iter.Current as String;
					int i = 0;
					foreach (CalComponent comp in event_list) {
                  
						Console.WriteLine ("item {0}", ++i);
						Console.WriteLine ("UId: {0}", comp.Uid);
						Console.WriteLine ("RecurId: {0}", comp.RecurId);
						Console.WriteLine ("Location: {0}", comp.Location);
						Console.WriteLine ("summary: {0}", comp.Summary);
						foreach (string str in comp.Categories)
							Console.WriteLine (str);
						foreach (string str in comp.Descriptions)
							Console.WriteLine (str);
						foreach (string str in comp.Comments)
							Console.WriteLine (str);
						Console.WriteLine ("Dtstarted: {0}", comp.Dtstart);
						Console.WriteLine ("Dtend: {0}", comp.Dtend);
						Console.WriteLine ("LastModified: {0}", comp.LastModified);
						Console.WriteLine ("Created: {0}", comp.Created);
						Console.WriteLine ("Complated: {0}", comp.Completed);
						Console.WriteLine ("Due: {0}", comp.Due);
						Console.WriteLine ("Classification: {0}", comp.Classification);
						Console.WriteLine ("Priority: {0}", comp.Priority);
						Console.WriteLine ("Status: {0}", comp.Status);
                  Console.WriteLine( "Attendee:" );
                  foreach (CalComponentAttendee att in comp.Attendees) {
                     Console.WriteLine ("value: \"{0}\"", att.value);
                     Console.WriteLine ("member: \"{0}\"", att.member);
                     Console.WriteLine ("cutype: \"{0}\"", att.cutype);
                     Console.WriteLine ("role: \"{0}\"", att.role);
                     Console.WriteLine ("status: \"{0}\"", att.status);
                     Console.WriteLine ("rsvp: \"{0}\"", att.rsvp);
                     Console.WriteLine ("delto: \"{0}\"", att.delto);
                     Console.WriteLine ("delfrom: \"{0}\"", att.delfrom);
                     Console.WriteLine ("sentby: \"{0}\"", att.sentby);
                     Console.WriteLine ("cn: \"{0}\"", att.cn);
                     Console.WriteLine ("language: \"{0}\"", att.language);
                     Console.WriteLine ("-----------");
                  }
		  
		  if (comp.RRules == null) {
			  Console.WriteLine ("No recurrence data found");
			  return;
		  }
		  foreach (CalRecurrence recur in comp.RRules) {
			  Console.WriteLine ("--- Recurrence data starts ---");

			  Console.WriteLine ("Frequeny:- {0}", recur.Frequency);
			  Console.WriteLine ("Count:- {0}", recur.Count);
			  Console.WriteLine ("Interval:- {0}", recur.Interval);
			  Console.WriteLine ("Enddate:- {0}", recur.Enddate);
			  Console.WriteLine ("BySecond:- {0}", recur.BySecond);
			  Console.WriteLine ("ByMinute:- {0}", recur.BySecond);
			  Console.WriteLine ("ByHour:- {0}", recur.ByHour);
			  Console.WriteLine ("ByDay:- {0}", recur.ByDay);
			  Console.WriteLine ("ByMonthDay:- {0}", recur.ByMonthDay);
			  Console.WriteLine ("ByYearDay:- {0}", recur.ByYearDay);
			  Console.WriteLine ("ByWeekNumber:- {0}", recur.ByWeekNumber);
			  Console.WriteLine ("ByMonth:- {0}", recur.ByMonth);
			  Console.WriteLine ("BySetPos:- {0}", recur.BySetPos);

			  Console.WriteLine ("--- Recurrence Data ends ---");
			  Console.WriteLine ("Recur string: \"{0}\"", recur.ToString());
		  }
#if false						
						if (comp.ExceptionRules != null) {
							Console.WriteLine ("No exception rules present");
							return;
						}
						foreach (CalRecurrence recur in comp.ExceptionRules) {
							Console.WriteLine ("--- Exception data starts ---");
									
							Console.WriteLine ("Frequeny:- {0}", recur.Frequency);
							Console.WriteLine ("Count:- {0}", recur.Count);
							Console.WriteLine ("Interval:- {0}", recur.Interval);
							Console.WriteLine ("Enddate:- {0}", recur.Enddate);
							Console.WriteLine ("BySecond:- {0}", recur.BySecond);
							Console.WriteLine ("ByMinute:- {0}", recur.BySecond);
							Console.WriteLine ("ByHour:- {0}", recur.ByHour);
							Console.WriteLine ("ByDay:- {0}", recur.ByDay);
							Console.WriteLine ("ByMonthDay:- {0}", recur.ByMonthDay);
							Console.WriteLine ("ByYearDay:- {0}", recur.ByYearDay);
							Console.WriteLine ("ByWeekNumber:- {0}", recur.ByWeekNumber);
							Console.WriteLine ("ByMonth:- {0}", recur.ByMonth);
							Console.WriteLine ("BySetPos:- {0}", recur.BySetPos);

							Console.WriteLine ("--- Exception Data ends ---");
						}
					}
#endif
            }
#endif			
#if false
					string change_id = "TestCal-testing";
					CalComponent[] newitems;
					CalComponent[] updated;
					string[] removed;

					//
					// First time when called, probably, will not return anything useful.
					// I don't think we need this API, however, got to double-check with
					// Joe on this.
					//
					cal.GetChanges (change_id, out newitems, out updated, out removed);
					Console.WriteLine ("Newly added: {0}", newitems.Length);
					Console.WriteLine ("Updated : {0}", updated.Length);
					Console.WriteLine ("removed added: {0}", removed.Length);
#endif
				}
			}
			Application.Run ();
		}
	}
}
